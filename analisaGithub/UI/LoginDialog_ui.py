# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/primary157/Universidade/Projeto/Nupessc/analisaGithub/analisaGithub/UI/LoginDialog.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(366, 168)
        self.verticalLayout = QtWidgets.QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.lineEditUser = QtWidgets.QLineEdit(Dialog)
        self.lineEditUser.setObjectName("lineEditUser")
        self.verticalLayout.addWidget(self.lineEditUser)
        self.lineEditPassword = QtWidgets.QLineEdit(Dialog)
        self.lineEditPassword.setEchoMode(QtWidgets.QLineEdit.Password)
        self.lineEditPassword.setObjectName("lineEditPassword")
        self.verticalLayout.addWidget(self.lineEditPassword)
        self.layoutDB = QtWidgets.QHBoxLayout()
        self.layoutDB.setObjectName("layoutDB")
        self.labelDB = QtWidgets.QLabel(Dialog)
        self.labelDB.setObjectName("labelDB")
        self.layoutDB.addWidget(self.labelDB)
        self.comboBoxDB = QtWidgets.QComboBox(Dialog)
        self.comboBoxDB.setObjectName("comboBoxDB")
        self.layoutDB.addWidget(self.comboBoxDB)
        self.verticalLayout.addLayout(self.layoutDB)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.dialogButtons = QtWidgets.QDialogButtonBox(Dialog)
        self.dialogButtons.setOrientation(QtCore.Qt.Horizontal)
        self.dialogButtons.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel | QtWidgets.QDialogButtonBox.Ok)
        self.dialogButtons.setObjectName("dialogButtons")
        self.verticalLayout.addWidget(self.dialogButtons)

        self.retranslateUi(Dialog)
        self.dialogButtons.accepted.connect(Dialog.accept)
        self.dialogButtons.rejected.connect(Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.lineEditUser.setPlaceholderText(_translate("Dialog", "Usuário"))
        self.lineEditPassword.setPlaceholderText(_translate("Dialog", "Senha"))
        self.labelDB.setText(_translate("Dialog", "Base de dados"))
