import time

from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QTableWidgetItem, QMessageBox, QHeaderView

from analisaGithub.Model.AnalysisParameter import AnalysisParameter
from analisaGithub.Model.AnalysisParameterType import AnalysisParameterType
from analisaGithub.Model.DataBaseService import DataBaseService
from analisaGithub.Model.Login import Login
from analisaGithub.Model.ProjectItemType import ProjectItemType
from analisaGithub.UI.MainWindow_ui import Ui_MainWindow
from analisaGithub.View.LoginDialog import LoginDialog
from analisaGithub.Controller.Seeker import Seeker
from analisaGithub.Controller.Counter import Counter
from threading import Thread


class MainWindow(Ui_MainWindow):
    def __init__(self):
        from PyQt5.QtWidgets import QApplication, QMainWindow
        import sys
        self.seeker = Seeker()
        self.counter = Counter()
        self.app = QApplication(sys.argv)
        self.login = LoginDialog(self, self.app)
        main_window = QMainWindow()
        self.setup_ui(main_window)
        main_window.show()
        sys.exit(self.app.exec_())

    def setup_ui(self, window):
        self.setupUi(window)
        self.setup_list()
        self.tabWidget.setTabEnabled(1, False)
        self.tabWidget.setTabEnabled(2, False)
        self.tabWidget.setTabEnabled(3, False)
        self.pushButton.clicked.connect(self.add_clicked)
        self.pushButton_Busca.clicked.connect(self.busca_clicked)
        self.pushButton_Contabilizacao.clicked.connect(self.contabilizacao_clicked)
        self.pushButton_Analise.clicked.connect(self.busca_clicked)
        self.actionNovo_Login.triggered.connect(self.new_login)
        self.pushButton_2.clicked.connect(self.add_project)
        self.progressBar.setValue(0)
        self.progressBar_2.setValue(0)
        self.progressBar_3.setValue(0)

    def add_clicked(self):
        self.counter.research_items.append((self.comboBox_2.currentText(),
                                            self.comboBox_3.currentText()))  # TODO: Há uma necessidade de elaborar melhor essa estrutura!!

    def setup_list(self):
        self.tableWidget.setColumnCount(5)
        self.tableWidget_3.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)
        self.tableWidget_3.horizontalHeader().setSectionResizeMode(1, QHeaderView.Stretch)
        self.tableWidget.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)
        self.tableWidget.setHorizontalHeaderLabels(["Repositório", "Commit", "Issue", "Pull Request", "File"])

    def new_login(self):
        self.login.show()

    @staticmethod
    def increase_progress(progressbar, pushbutton):
        curr = progressbar.value()
        if curr == 100:
            pushbutton.setText("Prosseguir >>")
        else:
            progressbar.setValue(curr + 10)

    def busca_progress_up(self):
        self.pushButton_Busca.setDisabled(True)
        for _ in range(11):
            time.sleep(1)
            self.increase_progress(self.progressBar, self.pushButton_Busca)
        self.pushButton_Busca.setDisabled(False)
        self.tabWidget.setTabEnabled((self.tabWidget.currentIndex() + 1), True)

    def busca_clicked(self):
        self.pushButton_Busca.clicked.disconnect()
        self.tableWidget_3.setItem(0, 2, QTableWidgetItem(QIcon(":/states/yellowlight.png"), ""))
        self.seeker.establish_connection_cvs()
        for idx, _ in enumerate(self.seeker.seek_projects()):
            self.tableWidget_3.setItem(idx, 2, QTableWidgetItem(QIcon(":/states/greenlight.png"), ""))
        # t = Thread(target=self.busca_progress_up)
        # t.start()
        self.pushButton_Busca.clicked.connect(lambda: self.tabWidget.setCurrentIndex(self.tabWidget.currentIndex() + 1))

    def contabilizacao_progress_up(self):
        self.pushButton_Contabilizacao.setDisabled(True)
        for _ in range(12):
            self.increase_progress(self.progressBar_2, self.pushButton_Contabilizacao)
            time.sleep(1)
        self.pushButton_Contabilizacao.setDisabled(False)
        self.tabWidget.setTabEnabled((self.tabWidget.currentIndex() + 1), True)

    def contabilizacao_clicked(self):
        self.pushButton_Contabilizacao.clicked.disconnect()
        self.counter.enumerate_artifact_related_items()
        t = Thread(target=self.contabilizacao_progress_up)
        t.start()
        self.pushButton_Contabilizacao.clicked.connect(
            lambda: self.tabWidget.setCurrentIndex(self.tabWidget.currentIndex() + 1))

    def analyze(self):
        print("oi")

    def enqueue_project(self, name, login):
        self.tableWidget_3.setRowCount(self.tableWidget_3.rowCount() + 1)
        self.tableWidget_3.setItem(self.tableWidget_3.rowCount() - 1, 0, QTableWidgetItem(name))
        self.tableWidget_3.setItem(self.tableWidget_3.rowCount() - 1, 1, QTableWidgetItem(login))
        self.tableWidget_3.setItem(self.tableWidget_3.rowCount() - 1, 2,
                                   QTableWidgetItem(QIcon(":/states/lightsoff.png"), ""))

    def add_project(self):
        try:
            owner, name = self.lineEditURL.text().split('/', 1)
        except ValueError:
            msg_box = QMessageBox()
            msg_box.setIcon(QMessageBox.Warning)
            msg_box.setWindowTitle("ValueError")
            msg_box.setText("Por favor preencha o campo de URL seguindo a seguinte sintaxe:")
            msg_box.setInformativeText("projectOwner/projectName")
            msg_box.setStandardButtons(QMessageBox.Ok)
            msg_box.exec_()
            self.lineEditURL.clear()
        else:
            self.seeker.projects.create_project(name, owner)
            try:
                self.seeker.logins.get_login(self.comboBox.currentText()).add_project(
                    self.seeker.projects.get_project(name))
            except KeyError:
                msg_box = QMessageBox()
                msg_box.setIcon(QMessageBox.Warning)
                msg_box.setWindowTitle("Nenhum Login selecionado")
                msg_box.setText("Selecione um login para acessar o projeto selecionado.")
                if self.comboBox.count() == 1:
                    msg_box.setInformativeText("Antes cadastre um login pressionando Novo Login.")
                msg_box.setStandardButtons(QMessageBox.Ok)
                msg_box.exec_()
            else:
                self.enqueue_project(owner + "/" + name, self.comboBox.currentText())


'''
if __name__ == "__main__":
    mw = MainWindow()
'''
if __name__ == '__main__':
    s = Seeker()
    s.logins.create_login('primary157', '', DataBaseService.GITHUB)
    s.projects.create_project('BusinessSysMan', 'primary157')
    l = s.logins.get_login('primary157')
    assert isinstance(l, Login)
    l.add_project(s.projects.get_project('BusinessSysMan'))
    s.establish_connection_cvs()
    s.seek_projects()
    c = Counter(s.projects)
    c.enumerate_artifact_related_items([AnalysisParameter('diagram', AnalysisParameterType.TEXT)],
                                       [ProjectItemType.ISSUE, ProjectItemType.COMMIT, ProjectItemType.FILE, ProjectItemType.PULL_REQUEST])
    print(input())
