from analisaGithub.Model.DataBaseService import DataBaseService
from analisaGithub.UI.LoginDialog_ui import Ui_Dialog
from PyQt5 import QtWidgets
import sys


class LoginDialog(Ui_Dialog):
    def __init__(self, mainwindow, app=QtWidgets.QApplication(sys.argv), dialog=QtWidgets.QDialog()):
        self.app = app
        self.dialog = dialog
        self.setup_ui(dialog)
        self.mainwindow = mainwindow

    def setup_ui(self, dialog):
        self.setupUi(dialog)
        for i in DataBaseService:
            self.comboBoxDB.addItem(i.name)
        self.dialogButtons.accepted.connect(self.confirm)
        #self.dialogButtons.rejected.connect(self.cancel)

    def confirm(self):
        if len(self.lineEditUser.text())  and len(self.lineEditPassword.text()):
            self.mainwindow.seeker.logins.create_login(self.lineEditUser.text(),self.lineEditPassword.text(), DataBaseService(self.comboBoxDB.currentIndex()+1))
        self.update_combobox()
        self.lineEditUser.clear()
        self.lineEditPassword.clear()
        self.comboBoxDB.setCurrentIndex(0)

    #def cancel(self):
    #    self.update_combobox()

    def update_combobox(self):
        self.mainwindow.comboBox.clear()
        self.mainwindow.comboBox.addItem("Login de acesso")
        for login in self.mainwindow.seeker.logins.get_logins():
            self.mainwindow.comboBox.addItem(login.username)

    def show(self):
        self.dialog.show()