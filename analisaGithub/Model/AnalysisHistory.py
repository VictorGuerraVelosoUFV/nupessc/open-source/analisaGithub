from datetime import datetime


class AnalysisHistory:
    def __init__(self, date):
        assert isinstance(date, datetime)
        self._date = date
        self._authors = set()
        self._n_mensions = 0

    @property
    def date(self):
        return self._date

    @date.setter
    def date(self, value):
        raise RuntimeError("You cannot modify the date of an analysis history")

    @property
    def authors(self):
        return self._authors

    @authors.setter
    def authors(self, value):
        assert isinstance(value, set) and all(isinstance(v, str) for v in value)
        self._authors = value

    @property
    def n_mensions(self):
        return self._n_mensions

    @n_mensions.setter
    def n_mensions(self, value):
        assert isinstance(value, int)
        self._n_mensions = value

    def add_author(self, author):
        assert isinstance(author, str)
        self.authors.add(author)

    def inc_mensions(self):
        self._n_mensions += 1
