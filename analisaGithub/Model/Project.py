import sys

from analisaGithub.Model.ProjectItem import ProjectItem
from analisaGithub.Model.ProjectItemType import ProjectItemType
from analisaGithub.Model.ProjectSyncState import ProjectSyncState
from analisaGithub.Model.RelevanceHistory import RelevanceHistory
from datetime import date


class Project:
    def __init__(self, name, owner, *, relevant=None, state=ProjectSyncState.IDLE):
        self._name = name
        self._owner = owner
        self._relevant = relevant
        self._state = state
        self._history = []
        self._project_items = {}

    @property
    def history(self):
        return self._history

    @property
    def name(self):
        return self._name

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, value):
        assert isinstance(value, str)
        self._owner = value

    @name.setter
    def name(self, value):
        assert isinstance(value, str)
        self._name = value

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, value):
        assert isinstance(value, ProjectSyncState)
        self._state = value

    @property
    def relevant(self):
        return self._relevant

    @relevant.setter
    def relevant(self, value):
        assert isinstance(value, bool)
        self._relevant = value

    @property
    def project_items(self):
        return self._project_items

    def __str__(self):
        return "name: {}\nowner: {}\nhistory: {{ {} }}\nitems: {{ {} }}".format(self.name, self._owner, self._history, self.project_items.values())

    def get_url(self):
        return self.owner + '/' + self.name

    def get_project_item(self, id):
        return self._project_items[id]

    def create_project_item(self, id: str, text: str, type: ProjectItemType, created_at: date, modified_at: list, author: str, *, relevant: bool = None) -> ProjectItem:
        assert all(isinstance(d, date) for d in modified_at)
        if id in self._project_items:
            print("Item de id: " + str(id) + " já existe!", file=sys.stderr)
            old = self._project_items[id]
            try:
                assert old.id == id
                assert old.text == text
                assert old.type == type
                assert old.created_at == created_at
                for d in modified_at:
                    assert d in old.modified_at
                assert old.author == author
            except AssertionError as e:
                print("Attributes' values do not match!")
                print(e)
                print("Ignoring...")
        else:
            self._project_items[id] = ProjectItem(type, id, text, author, created_at=created_at, modified_at=modified_at, relevant=relevant)
        return self._project_items[id]

    def rem_project_item(self, id):
        return self._project_items.pop(id)

    def set_project_relevant(self, id):
        item = self.get_project_item(id)
        for i in self._history:
            if i.date == date.today() and i.type == item.type:
                i.quantity += 1
                return
        self._history.append(RelevanceHistory(item.type, date.today()))
