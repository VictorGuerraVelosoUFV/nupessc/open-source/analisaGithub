from analisaGithub.Model.DataBaseService import DataBaseService
from analisaGithub.Model.Login import Login
from mysql.connector.cursor_cext import CMySQLCursor as Cursor

from analisaGithub.Model.Project import Project


class LoginDAO:
    def __init__(self, *, cursor=None):
        self._cursor = cursor

    def get_logins(self):
        self._cursor.execute("SELECT username, dbService, password FROM Login")
        return [Login(DataBaseService[db], username=username, password=password) for username, db, password in self._cursor.fetchall()]

    def create_login(self, username, password, db):
        self._cursor.execute("INSERT INTO Login (username, password, dbService) VALUES ('{}', '{}', '{}')".format(username, password, db))

    def update_login(self, username, db, **kwargs):
        attr = []
        login = Login(db, username=username)
        if 'new_username' in kwargs:
            attr.append("username = '{}'".format(kwargs['new_username']))
            login.username = kwargs['new_username']
        if 'new_db' in kwargs:
            attr.append("dbService = '{}'".format(kwargs['new_db']))
            login.db_service = kwargs['new_db']
        if 'new_password' in kwargs:
            attr.append("password = '{}'".format(kwargs['new_password']))
            login.password = kwargs['new_password']
        if len(attr) == 1:
            self.cursor.execute("UPDATE Login SET {}  WHERE username = '{}' AND dbService = '{}'".format(attr[0], username, db))
        elif len(attr) == 2:
            self.cursor.execute("UPDATE Login SET {}, {}  WHERE username = '{}' AND dbService = '{}'".format(attr[0], attr[1], username, db))
        elif len(attr) == 3:
            self.cursor.execute("UPDATE Login SET {}, {}, {}  WHERE username = '{}' AND dbService = '{}'".format(attr[0], attr[1], attr[2], username, db))
        else:
            raise ValueError("At least one optional parameter should be used")
        return login

    def delete_login(self, username, db):
        login = self.get_login(username, db)
        self.cursor.execute("DELETE FROM Login WHERE username = '{}' AND dbService = '{}'".format(username, db))
        return login

    def get_login(self, username, db):
        print(username)
        print(db)
        self.cursor.execute("SELECT * FROM Login WHERE username = '{}' AND dbService = '{}'".format(username, db))
        username, db, password = self.cursor.fetchone()
        login = Login(db, username=username)
        login.password = password
        return login

    def get_login_projects(self, username, db):
        self.cursor.execute("SELECT name, owner, relevant, state FROM Project WHERE loginUsername = '{}' AND dbService = '{}'".format(username, db))
        return [Project(name, owner, relevant=relevant, state=state) for name, owner, relevant, state in self.cursor.fetchall()]

    def set_cursor(self, cursor):
        assert isinstance(cursor, Cursor)
        self._cursor = cursor

    @property
    def cursor(self):
        return self._cursor
