from analisaGithub.Model.DataBaseService import DataBaseService
from analisaGithub.Model.Project import Project


class Login:
    def __init__(self, db, username="", password=""):
        self._username = username
        self._password = password
        self._projects = []
        self._db_service = db
    @property
    def username(self):
        return self._username

    @username.setter
    def username(self, value):
        assert isinstance(value,str)
        self._username = value

    @property
    def password(self):
        return self._password

    @password.setter
    def password(self, value):
        assert isinstance(value,str)
        self._password = value

    @property
    def db_service(self):
        return self._db_service

    @db_service.setter
    def db_service(self, value):
        assert isinstance(value,DataBaseService)
        self._db_service = value

    def add_project(self, project):
        assert isinstance(project, Project)
        project.login = self
        self._projects.append(project)

    @property
    def projects(self):
        return self._projects

    @projects.setter
    def projects(self, value):
        assert isinstance(value, list) and all(isinstance(item, Project) for item in value)
        self._projects = value
