from analisaGithub.Utils.SQLEnum import SQLEnum


class AnalysisParameterType(SQLEnum):
    TEXT = 1
    EXTENSIONS = 2
