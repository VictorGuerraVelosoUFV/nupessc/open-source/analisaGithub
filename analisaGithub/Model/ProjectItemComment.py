from datetime import datetime


class ProjectItemComment:
    def __init__(self, id, text=None, author=None, *, relevant=None, created_at=None, modified_at=None):
        self._id = id
        self._text = text
        self._relevant = relevant
        self._author = author
        self._created_at = created_at
        self._modified_at = modified_at

    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, value):
        assert isinstance(value, str)
        self._id = value

    @property
    def text(self):
        return self._text

    @text.setter
    def text(self, value):
        assert isinstance(value, str)
        self._text = value

    @property
    def relevant(self):
        return self._relevant

    @relevant.setter
    def relevant(self, value):
        assert isinstance(value, bool)
        self._relevant = value

    @property
    def author(self):
        return self._author

    @author.setter
    def author(self, value):
        assert isinstance(value, str)
        self._author = value

    @property
    def created_at(self):
        return self._created_at

    @created_at.setter
    def created_at(self, value):
        assert isinstance(value, datetime)
        self._created_at = value

    @property
    def modified_at(self):
        return self._modified_at

    @modified_at.setter
    def modified_at(self, value):
        assert isinstance(value, list) and all(isinstance(v, datetime) for v in value)
        self._modified_at = value
