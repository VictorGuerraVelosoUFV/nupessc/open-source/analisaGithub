from analisaGithub.Model.ProjectItemComment import ProjectItemComment
from analisaGithub.Model.ProjectItemType import ProjectItemType
from datetime import date


class ProjectItem:
    def __init__(self, type, id="", text="", author="", *, relevant=None, created_at=None, modified_at=None):
        assert isinstance(type, ProjectItemType)
        self._text = text.lower()  # Minimize number of comparison once everything is lower case
        self._id = id.lower()
        self._type = type
        self._correlated = {}
        self._comment = {}
        self._author = author
        self._relevant = relevant
        self._created_at = created_at
        self._modified_at = modified_at

    @property
    def comment(self):
        return self._comment

    @comment.setter
    def comment(self, value):
        raise RuntimeError("You should add or remove items, but not redefine the comment list")

    @property
    def author(self):
        return self._author

    @author.setter
    def author(self, value):
        assert isinstance(value, str)
        self._author = value

    @property
    def created_at(self):
        return self._created_at

    @created_at.setter
    def created_at(self, value):
        assert isinstance(value, date)
        self._created_at = value

    @property
    def modified_at(self):
        return self._modified_at

    @modified_at.setter
    def modified_at(self, value):
        assert isinstance(value, list) and all(isinstance(v, date) for v in value)
        self._modified_at = value

    @property
    def text(self):
        return self._text

    @text.setter
    def text(self, value):
        assert isinstance(value, str)
        self._text = value.lower()

    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, value):
        assert isinstance(value, str)
        self._id = value.lower()

    @property
    def type(self):
        return self._type

    @type.setter
    def type(self, value):
        assert isinstance(value, ProjectItemType)
        self._type = value

    @property
    def correlated(self):
        return self._correlated

    def add_correlated(self, item):
        assert isinstance(item, ProjectItem)
        self._correlated[item.id] = item

    def add_comment(self, comment):
        assert isinstance(comment, ProjectItemComment)
        self.comment[comment.id] = comment
        return comment

    @property
    def relevant(self):
        return self._relevant

    @relevant.setter
    def relevant(self, value):
        self._relevant = value

    def __str__(self):
        return "id: {}\ntype: {}\ntext: {}\ncorrelation: {{\n\t{} }}".format(self.id, self.type, self.text,
                                                                             "\n\t".join(self.correlated.values()))
