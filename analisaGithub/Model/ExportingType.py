from analisaGithub.Utils.SQLEnum import SQLEnum


class ExportingType(SQLEnum):
    JSON = 1
    PDF = 2
