from analisaGithub.Model.AnalysisParameterType import AnalysisParameterType
from nltk.corpus import wordnet as wn


class AnalysisParameter:
    def __init__(self, word, type):
        assert isinstance(word, str)
        assert isinstance(type, AnalysisParameterType)
        self.words = set()
        self.words.add(word.lower())
        self.type = type
        if type == AnalysisParameterType.TEXT:
            for y in wn.synsets(word, wn.NOUN):  # Capturando sinônimos em wordnet
                w = str(y).split("'")[1].split(".")[0]  # Removendo metadados retornados pelo serviço
                self.words.add(w.lower())
