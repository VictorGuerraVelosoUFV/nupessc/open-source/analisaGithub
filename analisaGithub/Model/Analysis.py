from datetime import datetime

from analisaGithub.Model.AnalysisHistory import AnalysisHistory
from analisaGithub.Model.ProjectItemType import ProjectItemType


class Analysis:
    def __init__(self, pn=""):
        self._projectName = pn
        self._items = {ProjectItemType.COMMIT: {ProjectItemType.COMMIT: 0, ProjectItemType.ISSUE: 0,
                                                ProjectItemType.PULL_REQUEST: 0, ProjectItemType.FILE: 0},
                       ProjectItemType.ISSUE: {ProjectItemType.COMMIT: 0, ProjectItemType.ISSUE: 0,
                                               ProjectItemType.PULL_REQUEST: 0, ProjectItemType.FILE: 0},
                       ProjectItemType.PULL_REQUEST: {ProjectItemType.COMMIT: 0, ProjectItemType.ISSUE: 0,
                                                      ProjectItemType.PULL_REQUEST: 0, ProjectItemType.FILE: 0},
                       ProjectItemType.FILE: {ProjectItemType.COMMIT: 0, ProjectItemType.ISSUE: 0,
                                              ProjectItemType.PULL_REQUEST: 0, ProjectItemType.FILE: 0}}
        self._history = {}

    def __getitem__(self, item):
        assert isinstance(item, ProjectItemType)
        return self._items[item]

    def __str__(self):
        return "".join(str(item) + ":\n\t" + "\n\t".join(
            str(i) + ": " + str(self.items[item][i]) + "\n" for i in self.items[item]) + "\n" for item in self.items)

    @property
    def items(self):
        return self._items

    @items.setter
    def items(self, value):
        pass  # do nothing

    @property
    def history(self):
        return self._history

    @history.setter
    def history(self, value):
        pass  # do nothing

    def add_history(self, date):
        assert isinstance(date, datetime)
        self._history[date] = AnalysisHistory(date)
        return self._history[date]
