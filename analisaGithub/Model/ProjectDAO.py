from analisaGithub.Controller.Connector import Connector
from analisaGithub.Model.LoginDAO import LoginDAO
from analisaGithub.Model.Project import Project
from mysql.connector.cursor_cext import CMySQLCursor as Cursor

from analisaGithub.Model.ProjectItem import ProjectItem
from analisaGithub.Model.ProjectItemComment import ProjectItemComment
from analisaGithub.Model.ProjectItemType import ProjectItemType


class ProjectDAO:
    def __init__(self, *, login_dao=None, cursor=None):
        if login_dao is None:
            login_dao = LoginDAO(cursor=cursor)
        elif login_dao is not None:
            assert isinstance(login_dao, LoginDAO)
        self._login_dao = login_dao
        self._project = {}
        self._cursor = cursor

    @property
    def project(self):
        return self._project

    @property
    def cursor(self):
        return self._cursor

    def create_project(self, name, owner):
        self._cursor.execute("INSERT INTO Project (name, owner) VALUES ('{}', '{}')".format(name, owner))
        return self.get_project(name, owner)

    def update_project(self, project, recursive=False, **kwargs):
        attr = []
        name = project.name
        owner = project.owner
        if 'new_name' in kwargs:
            attr.append("name = '{}'".format(kwargs['new_name']))
            project.name = kwargs['new_name']
        if 'new_owner' in kwargs:
            attr.append("owner = '{}'".format(kwargs['new_owner']))
            project.owner = kwargs['new_owner']
        if 'new_relevant' in kwargs:
            attr.append("relevant = '{}'".format(int(kwargs['new_relevant'])))
            project.relevant = kwargs['new_relevant']
        if 'new_state' in kwargs:
            attr.append("state = '{}'".format(kwargs['new_state']))
            project.state = kwargs['new_state']
        if 'login_username' in kwargs or 'login_db' in kwargs:
            if 'login_username' in kwargs and 'login_db' in kwargs:
                attr.append("loginUsername = '{}'".format(kwargs['login_username']))
                attr.append("dbService = '{}'".format(kwargs['login_db']))
            else:
                raise ValueError("One cannot modify one of login_username and login_db without modifying the other")
        if len(attr) == 1:
            self._cursor.execute("UPDATE Project SET {}  WHERE name = '{}' AND owner = '{}'".format(attr[0], name, owner))
        elif len(attr) == 2:
            self._cursor.execute("UPDATE Project SET {}, {}  WHERE name = '{}' AND owner = '{}'".format(attr[0], attr[1], name, owner))
        elif len(attr) == 3:
            self._cursor.execute("UPDATE Project SET {}, {}, {}  WHERE name = '{}' AND owner = '{}'".format(attr[0], attr[1], attr[2], name, owner))
        elif len(attr) == 4:
            self._cursor.execute("UPDATE Project SET {}, {}, {}, {}  WHERE name = '{}' AND owner = '{}'".format(attr[0], attr[1], attr[2], attr[3], name, owner))
        elif len(attr) == 5:
            self._cursor.execute("UPDATE Project SET {}, {}, {}, {}, {}  WHERE name = '{}' AND owner = '{}'".format(attr[0], attr[1], attr[2], attr[3], attr[4], name, owner))
        elif len(attr) == 6:
            self._cursor.execute("UPDATE Project SET {}, {}, {}, {}, {}, {}  WHERE name = '{}' AND owner = '{}'".format(attr[0], attr[1], attr[2], attr[3], attr[4], attr[5], name, owner))
        else:
            if not recursive:
                raise ValueError("At least one optional parameter should be used")
        if recursive:
            for id, item in project.project_items:
                pass
        return project

    def delete_project(self, name, owner):
        project = self.get_project(name, owner)
        self._cursor.execute("DELETE FROM Project WHERE name = '{}' AND owner = '{}'".format(name, owner))
        return project

    def get_project(self, name, owner):
        self._cursor.execute("SELECT name, owner, relevant, state, loginUsername, dbService FROM Project WHERE name = '{}' AND owner = '{}'".format(name, owner))
        name, owner, relevant, state, _, _ = self._cursor.fetchone()
        return Project(name, owner, relevant=relevant, state=state)

    def get_projects(self):
        self._cursor.execute("SELECT name, owner FROM Project")
        return [Project(name, owner) for name, owner in self._cursor.fetchall()]

    def _add_project_item(self, project, id, text, type, created_at, modified_at, author, *, relevant=None):
        self._cursor.execute("INSERT INTO ProjectItemModification (id) VALUES (NULL)")
        Connector.db.commit()
        self._cursor.execute("SELECT id FROM ProjectItemModification ORDER BY id DESC LIMIT 1")
        mod_id = self._cursor.fetchone()[0]
        for date in modified_at:
            self._cursor.execute("INSERT INTO ModificationDate (date, projectItemModificationId) VALUES ('{}', '{}')".format(date, mod_id))
        Connector.db.commit()
        if relevant is None:
            self._cursor.execute(
                "INSERT INTO ProjectItem (id, projectName, projectOwner, type, projectItemModificationId, createdAt, text, author) VALUES ('{}', '{}','{}', '{}','{}', '{}','{}', '{}')".format(
                    id, project.name, project.owner, type, mod_id, created_at, text, author))
        else:
            self._cursor.execute(
                "INSERT INTO ProjectItem (id, projectName, projectOwner, type, projectItemModificationId, createdAt, text, author, relevant) VALUES ('{}', '{}','{}', '{}','{}', '{}','{}', '{}', '{}')".format(
                    id, project.name, project.owner, type, mod_id, created_at, text, author, relevant))
        return project.create_project_item(id, text, type, created_at, modified_at, author, relevant=relevant)

    def _get_project_item(self, project, id):
        self._cursor.execute(
            "SELECT id, type, projectItemModificationId, createdAt, text, author, relevant FROM ProjectItem WHERE id = '{}' AND projectName = '{}' AND projectOwner = '{}'".format(id, project.name, project.owner))
        id, type, project_item_modification_id, created_at, text, author, relevant = self._cursor.fetchone()
        self._cursor.execute("SELECT date FROM ModificationDate WHERE projectItemModificationId = '{}'".format(project_item_modification_id))
        modified_at = [d[0] for d in self._cursor.fetchall()]  # tuple of date to date
        return project.create_project_item(id, text, ProjectItemType[type], created_at, modified_at, author, relevant=relevant)

    def _get_project_items(self, project):
        self._cursor.execute("SELECT id FROM ProjectItem WHERE projectName = '{}' AND projectOwner = '{}'".format(project.name, project.owner))
        return [self._get_project_item(project, i[0]) for i in self._cursor.fetchall()] # tuple of id to id

    def _update_project_item(self, project, id, **kwargs):
        attr = []
        self._cursor.execute(
            "SELECT id, type, projectItemModificationId, createdAt, text, author, relevant FROM ProjectItem WHERE id = '{}' AND projectName = '{}' AND projectOwner = '{}'".format(id, project.name, project.owner))
        item_id, type, project_item_modification_id, created_at, text, author, relevant = self._cursor.fetchone()
        self._cursor.execute("SELECT date FROM ModificationDate WHERE projectItemModificationId = '{}'".format(project_item_modification_id))
        modified_at = [d[0] for d in self._cursor.fetchall()]  # tuple of date to date
        item = ProjectItem(ProjectItemType[type], item_id, text, author, relevant=relevant, created_at=created_at, modified_at=modified_at)
        if 'new_text' in kwargs:
            attr.append("text = '{}'".format(kwargs['new_text']))
            item.text = kwargs['new_text']
        if 'new_id' in kwargs:
            attr.append("id = '{}'".format(kwargs['new_id']))
            item.id = kwargs['new_id']
        if 'new_type' in kwargs:
            attr.append("type = '{}'".format(kwargs['new_type']))
            item.type = kwargs['new_type']
        if 'new_relevant' in kwargs:
            attr.append("relevant = '{}'".format(int(kwargs['new_relevant'])))
            item.relevant = kwargs['new_relevant']
        if 'new_modified_at' in kwargs:
            # attr.append("modifiedAt = '{}'".format(kwargs['new_modified_at']))
            self._cursor.execute("DELETE FROM ModificationDate WHERE projectItemModificationId = '{}'".format(project_item_modification_id))
            for d in kwargs['new_modified_at']:
                self._cursor.execute("INSERT INTO ModificationDate (date, projectItemModificationId) VALUES ('{}', '{}')".format(d, project_item_modification_id))
            item.modified_at = kwargs['new_modified_at']
        if 'new_author' in kwargs:
            attr.append("author = '{}'".format(kwargs['new_author']))
            item.author = kwargs['new_author']
        if len(attr) == 1:
            self._cursor.execute("UPDATE ProjectItem SET {}  WHERE id = '{}' AND projectName = '{}' AND projectOwner = '{}'".format(attr[0], id, project.name, project.owner))
        elif len(attr) == 2:
            self._cursor.execute("UPDATE ProjectItem SET {}, {}  WHERE id = '{}' AND projectName = '{}' AND projectOwner = '{}'".format(attr[0], attr[1], id, project.name, project.owner))
        elif len(attr) == 3:
            self._cursor.execute("UPDATE ProjectItem SET {}, {}, {}  WHERE id = '{}' AND projectName = '{}' AND projectOwner = '{}'".format(attr[0], attr[1], attr[2], id, project.name, project.owner))
        elif len(attr) == 4:
            self._cursor.execute("UPDATE ProjectItem SET {}, {}, {}, {}  WHERE id = '{}' AND projectName = '{}' AND projectOwner = '{}'".format(attr[0], attr[1], attr[2], attr[3], id, project.name, project.owner))
        elif len(attr) == 5:
            self._cursor.execute(
                "UPDATE ProjectItem SET {}, {}, {}, {}, {}  WHERE id = '{}' AND projectName = '{}' AND projectOwner = '{}'".format(attr[0], attr[1], attr[2], attr[3], attr[4], id, project.name, project.owner))
        else:
            raise ValueError("At least one optional parameter should be used")
        return item

    def _rem_project_item(self, project, id):
        self._cursor.execute("SELECT id, projectItemModificationId FROM ProjectItem WHERE id = '{}' AND projectName = '{}' AND projectOwner = '{}'".format(id, project.name, project.owner))
        id, project_item_modification_id = self._cursor.fetchone()
        self._cursor.execute("DELETE FROM ProjectItemModification WHERE id = '{}'".format(project_item_modification_id))

    def add_correlation(self, project_name, project_owner, item_id, correlated_id):
        self._cursor.execute(
            "INSERT INTO ProjectItemCorrelation (itemId, itemProjectName, itemProjectOwner, correlatedId, correlatedProjectName, correlatedProjectOwner) VALUES ('{0}', '{1}','{2}', '{3}','{1}', '{2}')".format(
                item_id, project_name, project_owner, correlated_id))

    def add_comment(self, project_name, project_owner, item_id, id, text, created_at, modified_at, author):
        self._cursor.execute("INSERT INTO ProjectItemModification (id) VALUES (NULL)")
        Connector.db.commit()
        self._cursor.execute("SELECT id FROM ProjectItemModification ORDER BY id DESC LIMIT 1")
        mod_id = self._cursor.fetchone()[0]
        for date in modified_at:
            self._cursor.execute("INSERT INTO ModificationDate (date, projectItemModificationId) VALUES ('{}', '{}')".format(date, mod_id))
        Connector.db.commit()
        self._cursor.execute(
            "INSERT INTO ProjectItemComment (id, projectItemId, projectName, projectOwner, text, createdAt, author, projectItemModificationId) VALUES ('{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}')".format(
                id, item_id, project_name, project_owner, text, created_at, author, mod_id))

    def get_correlateds(self, project_name, project_owner, id):
        self._cursor.execute("SELECT correlatedId FROM ProjectItemCorrelation WHERE itemId = '{}' AND itemProjectName = '{}' AND itemProjectOwner = '{}'".format(id, project_name, project_owner))
        correlated_ids = [d[0] for d in self._cursor.fetchall()]
        self._cursor.execute("SELECT itemId FROM ProjectItemCorrelation WHERE correlatedId = '{}' AND itemProjectName = '{}' AND itemProjectOwner = '{}'".format(id, project_name, project_owner))
        correlated_ids += [d[0] for d in self._cursor.fetchall()]
        return [self._get_project_item(Project(project_name, project_owner), correlated_id) for correlated_id in correlated_ids]

    def get_comments(self, project_name, project_owner, id):
        self._cursor.execute(
            "SELECT id, text, relevant, createdAt, author, projectItemModificationId FROM ProjectItemComment WHERE projectName = '{}' AND projectOwner = '{}' AND projectItemId = '{}'".format(project_name,
                                                                                                                                                                                               project_owner, id))
        comments_data = self._cursor.fetchall()
        comments = []
        for id, text, relevant, created_at, author, project_item_modification_id in comments_data:
            self._cursor.execute("SELECT date FROM ModificationDate WHERE projectItemModificationId = '{}'".format(project_item_modification_id))
            modification_dates = [d[0] for d in self._cursor.fetchall()]
            comments.append(ProjectItemComment(id, text, author, relevant=relevant, created_at=created_at, modified_at=modification_dates))
        return comments

    def add_project_to_login(self, username, db, project_name, project_owner):
        self.update_project(project_name, project_owner, login_username=username, login_db=db)

    def set_cursor(self, cursor):
        assert isinstance(cursor, Cursor)
        self._cursor = cursor
