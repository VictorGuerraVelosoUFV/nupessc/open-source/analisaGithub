class RelevanceHistory:
    def __init__(self, type, date):
        self.date = date
        self.quantity = 1
        self.type = type

    def __str__(self):
        return "date: {}\nquantity: {}\ntype: {}".format(self.date, self.quantity, str(self.type))
