from analisaGithub.Utils.SQLEnum import SQLEnum


class ProjectItemType(SQLEnum):
    COMMIT = 1
    ISSUE = 2
    PULL_REQUEST = 3
    FILE = 4
