from analisaGithub.Utils.SQLEnum import SQLEnum


class ProjectSyncState(SQLEnum):
    IDLE = 1
    SYNCING = 2
    FAILED = 3
    SUCCEEDED = 4
