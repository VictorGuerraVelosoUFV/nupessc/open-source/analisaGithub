from enum import IntEnum


class SQLEnum(IntEnum):
    def __str__(self):
        return super().__str__().split(sep='.')[-1]

    def __format__(self, format_spec):
        return str(self)
