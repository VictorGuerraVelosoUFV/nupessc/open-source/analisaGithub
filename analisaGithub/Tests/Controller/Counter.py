import datetime

from analisaGithub.Controller.Seeker import Seeker

from analisaGithub.Controller.Counter import Counter
import unittest

from analisaGithub.Model.AnalysisParameter import AnalysisParameter
from analisaGithub.Model.AnalysisParameterType import AnalysisParameterType
from analisaGithub.Model.DataBaseService import DataBaseService
from analisaGithub.Model.ProjectItemType import ProjectItemType


class TestCounter(unittest.TestCase):
    def setUp(self):
        self.seeker = Seeker()
        proj_name = 'BusinessSysMan'
        username = 'testFOSSResearch'
        self.seeker.logins.create_login(username, 'FOSSResearchtest', DataBaseService.GITHUB)
        self.seeker.projects.create_project(proj_name, username)
        l = self.seeker.logins.get_login(username)
        l.add_project(self.seeker.projects.get_project(proj_name))
        self.seeker.establish_connection_cvs()
        self.seeker.seek_projects()
        self.counter = Counter(self.seeker.projects)

    def tearDown(self):
        import os
        try:
            os.remove("arquivo.dat")
        except FileNotFoundError:
            pass

    def test_enumeration(self):
        self.counter.enumerate_artifact_related_items([AnalysisParameter('diagram', AnalysisParameterType.TEXT)],
                                                      [ProjectItemType.PULL_REQUEST, ProjectItemType.ISSUE,
                                                       ProjectItemType.COMMIT, ProjectItemType.FILE])
        for project in (self.seeker.projects.get_project(name) for name in self.seeker.projects.get_projects()):
            self.assertTrue(((history.type, history.date, history.quantity) for history in project.history), (
                (ProjectItemType.FILE, datetime.date.today(), 36),
                (ProjectItemType.COMMIT, datetime.date.today(), 5),
                (ProjectItemType.PULL_REQUEST, datetime.date.today(), 1),
                (ProjectItemType.ISSUE, datetime.date.today(), 2)))
