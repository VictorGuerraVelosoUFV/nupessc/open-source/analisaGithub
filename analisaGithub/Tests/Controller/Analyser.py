from unittest import TestCase

from analisaGithub.Model.Login import Login
from analisaGithub.Model.Project import Project

from analisaGithub.Controller.Counter import Counter
from analisaGithub.Controller.Seeker import Seeker

from analisaGithub.Controller.Analyser import Analyser
from analisaGithub.Model.AnalysisParameter import AnalysisParameter
from analisaGithub.Model.AnalysisParameterType import AnalysisParameterType
from analisaGithub.Model.DataBaseService import DataBaseService
from analisaGithub.Model.ProjectItem import ProjectItem
from analisaGithub.Model.ProjectItemType import ProjectItemType


class TestAnalyser(TestCase):
    def setUp(self):
        self.seeker = Seeker()
        # creating good login
        self.seeker.logins.create_login("testFOSSResearch", "FOSSResearchtest", DataBaseService.GITHUB)
        self.seeker.projects.create_project("BusinessSysMan", "primary157")
        l = self.seeker.logins.get_login('testFOSSResearch')
        assert isinstance(l, Login)
        l.add_project(self.seeker.projects.get_project('BusinessSysMan'))
        self.seeker.establish_connection_cvs()
        self.seeker.seek_projects()
        self.counter = Counter(self.seeker.projects)
        self.counter.enumerate_artifact_related_items([AnalysisParameter('diagram', AnalysisParameterType.TEXT)],
                                                      [ProjectItemType.ISSUE, ProjectItemType.COMMIT,
                                                       ProjectItemType.FILE,
                                                       ProjectItemType.PULL_REQUEST])

    def tearDown(self):
        import os
        try:
            os.remove("arquivo.dat")
        except FileNotFoundError:
            pass

    def test_correlate_items(self):
        a = Analyser(self.counter.proj_dao)
        for p_name in a.proj_dao.get_projects():
            p = a.proj_dao.get_project(p_name)
            a.correlate_items(p)
            self.assertIsInstance(p, Project)
            for i in p.project_items.values():
                if i.type == ProjectItemType.ISSUE:
                    self.assertTrue(i.id == "6" or i.id == "2" or i.id == "1")

    def test_generate_analysis(self):
        r_items = {ProjectItemType.COMMIT: {ProjectItemType.COMMIT: 0, ProjectItemType.ISSUE: 0,
                                            ProjectItemType.PULL_REQUEST: 3, ProjectItemType.FILE: 71},
                   ProjectItemType.ISSUE: {ProjectItemType.COMMIT: 0, ProjectItemType.ISSUE: 0,
                                           ProjectItemType.PULL_REQUEST: 2, ProjectItemType.FILE: 0},
                   ProjectItemType.PULL_REQUEST: {ProjectItemType.COMMIT: 3, ProjectItemType.ISSUE: 2,
                                                  ProjectItemType.PULL_REQUEST: 0, ProjectItemType.FILE: 0},
                   ProjectItemType.FILE: {ProjectItemType.COMMIT: 71, ProjectItemType.ISSUE: 0,
                                          ProjectItemType.PULL_REQUEST: 0, ProjectItemType.FILE: 0}}
        items = {ProjectItemType.COMMIT: {ProjectItemType.COMMIT: 0, ProjectItemType.ISSUE: 0,
                                          ProjectItemType.PULL_REQUEST: 7, ProjectItemType.FILE: 202},
                 ProjectItemType.ISSUE: {ProjectItemType.COMMIT: 0, ProjectItemType.ISSUE: 0,
                                         ProjectItemType.PULL_REQUEST: 2, ProjectItemType.FILE: 0},
                 ProjectItemType.PULL_REQUEST: {ProjectItemType.COMMIT: 7, ProjectItemType.ISSUE: 2,
                                                ProjectItemType.PULL_REQUEST: 0, ProjectItemType.FILE: 0},
                 ProjectItemType.FILE: {ProjectItemType.COMMIT: 202, ProjectItemType.ISSUE: 0,
                                        ProjectItemType.PULL_REQUEST: 0, ProjectItemType.FILE: 0}}
        a = Analyser(self.counter.proj_dao)
        a.generate_analysis()
        for k, v in zip(a.analysis.keys(), a.analysis.values()):
            if k[-2:] == '.r':  # relevant values
                self.assertEqual(v.items, r_items)
            else:
                self.assertEqual(v.items, items)
