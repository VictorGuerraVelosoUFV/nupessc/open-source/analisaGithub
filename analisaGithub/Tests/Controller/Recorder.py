from json import load
from unittest import TestCase, skip

from analisaGithub.Controller.Analyser import Analyser
from analisaGithub.Controller.Counter import Counter
from analisaGithub.Controller.ExportingType import ExportingType
from analisaGithub.Controller.Recorder import Recorder
from analisaGithub.Controller.Seeker import Seeker
from analisaGithub.Model.AnalysisParameter import AnalysisParameter
from analisaGithub.Model.AnalysisParameterType import AnalysisParameterType
from analisaGithub.Model.DataBaseService import DataBaseService
from analisaGithub.Model.Login import Login
from analisaGithub.Model.ProjectItemType import ProjectItemType


class TestRecorder(TestCase):
    def setUp(self):
        self.seeker = Seeker()
        self.seeker.logins.create_login("testFOSSResearch", "FOSSResearchtest", DataBaseService.GITHUB)
        self.seeker.projects.create_project('BusinessSysMan', 'primary157')
        l = self.seeker.logins.get_login('testFOSSResearch')
        assert isinstance(l, Login)
        l.add_project(self.seeker.projects.get_project('BusinessSysMan'))
        self.seeker.establish_connection_cvs()

    def tearDown(self):
        import os
        from glob import glob
        try:
            files = glob("./*teste*json")
            for file in files:
                os.remove(file)
            os.remove("arquivo.dat")
        except FileNotFoundError:
            pass

    def _test_record_wo_seeking(self):
        r = Recorder(Analyser(Seeker().projects))
        with self.assertRaises(RuntimeError):
            r.record(ExportingType.JSON)

    def test_record_w_seeking(self):
        self.seeker.seek_projects()
        r = Recorder(Analyser(self.seeker.projects))
        r.record(ExportingType.JSON)
        with open('steste0.json.bkp') as file:
            f = load(file)
        for name in self.seeker.projects.get_projects():
            p = self.seeker.projects.get_project(name)
            for h in p.history:
                for i, j in zip((h.type, h.date, h.quantity), f.values()):
                    self.assertEqual(i, j)

    def test_record_w_counting(self):
        self.seeker.seek_projects()
        c = Counter(self.seeker.projects)
        c.enumerate_artifact_related_items([AnalysisParameter('diagram', AnalysisParameterType.TEXT)],
                                           [ProjectItemType.ISSUE, ProjectItemType.COMMIT, ProjectItemType.FILE,
                                            ProjectItemType.PULL_REQUEST])
        r = Recorder(Analyser(c.proj_dao))
        r.record(ExportingType.JSON)
        for name in c.proj_dao.get_projects():
            for idx, h in enumerate(c.proj_dao.get_project(name).history):
                with open('cteste0' + str(idx) + '.json.bkp') as file:
                    f = load(file)
                for i, j in zip(f.values(), (str(h.type), str(h.date), str(h.quantity))):
                    self.assertEqual(i, j)

    def test_record_w_analysing(self):
        self.seeker.seek_projects()
        c = Counter(self.seeker.projects)
        c.enumerate_artifact_related_items([AnalysisParameter('diagram', AnalysisParameterType.TEXT)],
                                           [ProjectItemType.ISSUE, ProjectItemType.COMMIT, ProjectItemType.FILE,
                                            ProjectItemType.PULL_REQUEST])
        a = Analyser(c.proj_dao)
        a.generate_analysis()
        r = Recorder(a)
        r.record(ExportingType.JSON)
        for n, analysis in enumerate(a.analysis.values()):
            with open('ateste' + str(n) + '.json.bkp') as file:
                f = load(file)
                for k, l in zip((m.values() for m in analysis.items.values()), (n.values() for n in f.values())):
                    for i, j in zip(k, l):
                        self.assertEqual(i, j)
