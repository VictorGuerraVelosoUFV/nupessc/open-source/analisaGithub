from github import BadCredentialsException

from analisaGithub.Controller.Connector import Connector
from analisaGithub.Controller.Seeker import Seeker
import unittest

from analisaGithub.Model.DataBaseService import DataBaseService
from analisaGithub.Model.Login import Login
from analisaGithub.Model.LoginDAO import LoginDAO
from analisaGithub.Model.Project import Project
from analisaGithub.Model.ProjectDAO import ProjectDAO
from analisaGithub.Model.ProjectItemType import ProjectItemType


class TestSeeker(unittest.TestCase):
    def setUp(self):
        self.proj_name = 'BusinessSysMan'
        self.proj_owner = 'primary157'
        self.ghusername = 'testFOSSResearch'
        self.ghpasswd = 'FOSSResearchtest'
        self.username = "victorgv"
        self.passwd = "12345"
        self.dbhost = 'localhost'
        self.dbusername = 'victorgv'
        self.dbpasswd = '12345'
        self.dbname = 'mydb'

    def tearDown(self):
        import os
        try:
            os.remove("arquivo.dat")
        except FileNotFoundError:
            pass

    def test_establishing_connection_bad_credentials(self):
        proj_dao = ProjectDAO()
        test_proj = Project("test", "testFOSSResearch")
        log_dao = LoginDAO()
        log_dao.create_login("badCredentialUsername", "badCredentialPassword", DataBaseService.GITHUB)
        test_log = log_dao.get_login("badCredentialUsername", DataBaseService.GITHUB)
        test_log.add_project(test_proj)
        seeker = Seeker(login_dao=log_dao, project_dao=proj_dao)
        with self.assertRaises(BadCredentialsException):
            seeker.establish_connection_cvs()

    def test_project_seeking_wo_connection(self):
        s = Seeker()
        with self.assertRaises(RuntimeError):
            s.seek_projects()

    def test_seek_1_project(self):
        seeker = Seeker()
        seeker.establish_connection_db(self.dbhost, self.dbusername, self.dbpasswd, self.dbname)
        seeker.logins.create_login(self.ghusername, self.ghpasswd, DataBaseService.GITHUB)
        seeker.establish_connection_cvs()
        seeker.projects.create_project(self.proj_name, self.proj_owner)
        seeker.projects.add_project_to_login(self.ghusername, DataBaseService.GITHUB, self.proj_name, self.proj_owner)
        seeker.seek_projects()

    @unittest.skip("WIP")
    def test_seek_many_projects(self):
        seeker = Seeker()
        seeker.establish_connection_db(self.dbhost, self.dbusername, self.dbpasswd, self.dbname)
        seeker.logins.create_login(self.ghusername, self.ghpasswd, DataBaseService.GITHUB)
        seeker.establish_connection_cvs()
        seeker.projects.create_project(self.proj_name, self.proj_owner)
        seeker.projects.add_project_to_login(self.ghusername, DataBaseService.GITHUB, self.proj_name, self.proj_owner)

    def test_login_db(self):
        s = Seeker()
        s.establish_connection_db('localhost', self.username, self.passwd, 'mydb')
        s.logins.create_login(self.ghusername, self.ghpasswd, DataBaseService.GITHUB)
        s.projects.create_project(self.proj_name, self.proj_owner)
        l = s.logins.get_login(self.ghusername)
        l.add_project(s.projects.get_project(self.proj_name))
        s.establish_connection_cvs()
        Connector.db.commit()
