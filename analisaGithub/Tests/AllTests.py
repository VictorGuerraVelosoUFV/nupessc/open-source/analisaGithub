import unittest
from analisaGithub.Tests.Controller import Seeker, Counter, Analyser, Recorder
from analisaGithub.Tests.Model import ProjectDAOProject, ProjectDAOProjectItem, LoginDAO, ProjectLogin


def all_tests_suite():
    # init
    loader = unittest.TestLoader()
    suite = unittest.TestSuite()
    # runner = unittest.TextTestRunner(verbosity=3)
    # add test modules
    suite.addTest(loader.loadTestsFromModule(ProjectDAOProject))
    suite.addTest(loader.loadTestsFromModule(ProjectDAOProjectItem))
    suite.addTest(loader.loadTestsFromModule(ProjectLogin))
    suite.addTest(loader.loadTestsFromModule(LoginDAO))
    suite.addTest(loader.loadTestsFromModule(Seeker))
    # suite.addTest(loader.loadTestsFromModule(Analyser))
    # suite.addTest(loader.loadTestsFromModule(Counter))
    # suite.addTest(loader.loadTestsFromModule(Recorder))
    # run tests
    # res = runner.run(suite)
    # print results
    # print(res)
    return suite


def run_all_tests_suite():
    # init
    loader = unittest.TestLoader()
    suite = unittest.TestSuite()
    runner = unittest.TextTestRunner(verbosity=3)
    # add test modules
    suite.addTest(loader.loadTestsFromModule(ProjectDAOProject))
    suite.addTest(loader.loadTestsFromModule(ProjectDAOProjectItem))
    suite.addTest(loader.loadTestsFromModule(ProjectLogin))
    suite.addTest(loader.loadTestsFromModule(LoginDAO))
    suite.addTest(loader.loadTestsFromModule(Seeker))
    # suite.addTest(loader.loadTestsFromModule(Counter))
    # suite.addTest(loader.loadTestsFromModule(Analyser))
    # suite.addTest(loader.loadTestsFromModule(Recorder))
    # run tests
    res = runner.run(suite)
    # print results
    print(res)


if __name__ == "__main__":
    run_all_tests_suite()
