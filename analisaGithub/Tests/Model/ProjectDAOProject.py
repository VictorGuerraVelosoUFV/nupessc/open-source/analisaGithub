import random
import re
import sys
from datetime import date
from unittest import TestCase

from faker import Faker

from analisaGithub.Controller.Connector import Connector
from analisaGithub.Controller.Seeker import Seeker
from analisaGithub.Model.DataBaseService import DataBaseService
from analisaGithub.Model.ProjectItemType import ProjectItemType
from analisaGithub.Model.ProjectSyncState import ProjectSyncState


class TestProjectDAO(TestCase):
    def setUp(self):
        self.proj_name = 'BusinessSysMan'
        self.proj_owner = 'primary157'
        self.proj_state = ProjectSyncState.IDLE
        self.username = "victorgv"
        self.passwd = "12345"
        self.db_name = 'mydb'
        self.fake = Faker()
        self.seeker = Seeker()
        self.seeker.establish_connection_db('localhost', self.username, self.passwd, self.db_name)
        self.old_username = "fadoa"
        self.password = "glauss"
        self.new_proj_relevant = False
        self.new_proj_name = "BSM"
        self.new_proj_owner = "messiahglauss"
        self.new_proj_state = ProjectSyncState.SYNCING
        self.db = DataBaseService.GITHUB
        self.seeker.logins.cursor.execute("INSERT INTO Login (username, password, dbService) VALUES ('{}', '{}', '{}')".format(self.old_username, self.password, self.db))
        Connector.db.commit()
        self.seeker.logins.cursor.execute("INSERT INTO Project (name, owner) VALUES ('{}', '{}')".format(self.proj_name, self.proj_owner))
        Connector.db.commit()

    def tearDown(self):
        import os
        try:
            os.remove("arquivo.dat")
        except FileNotFoundError:
            pass
        self.seeker.logins.cursor.execute("DROP SCHEMA mydb;")
        Connector.db.commit()
        with open('../DER.sql') as f:
            self.seeker.logins.cursor.execute(f.read(), multi=True)

    def test_create_project(self):
        proj_name = "projeto1"
        proj_owner = "fadoa"
        self.seeker.projects.create_project(proj_name, proj_owner)
        Connector.db.commit()
        self.seeker.projects.cursor.execute("SELECT name, owner, state FROM Project WHERE name = '{}' AND owner = '{}'".format(proj_name, proj_owner))
        try:
            name, owner, state = self.seeker.projects.cursor.fetchone()
            self.assertEqual(proj_name, name)
            self.assertEqual(proj_owner, owner)
            self.assertEqual(ProjectSyncState.IDLE, ProjectSyncState[state])
        except (ValueError, IndexError, TypeError):
            self.fail("SELECT não encontrou '{}'".format(proj_name))

    def test_project_w_items(self):
        proj_name = "projeto1"
        proj_owner = "fadoa"
        proj = self.seeker.projects.create_project(proj_name, proj_owner)
        Connector.db.commit()
        item_type = random.choice(list(ProjectItemType))
        item_text = self.fake.sentence(nb_words=30, variable_nb_words=True, ext_word_list=None)
        item_id = str(random.randint(0, sys.maxsize - 1))
        item_created_at = self.fake.past_date(start_date="-900d", tzinfo=None)
        item_modified_at = [self.fake.past_date(start_date=str((item_created_at - date.today()).days) + 'd', tzinfo=None)]
        item_author = re.sub(' ', '', self.fake.name()) + "@example.com"
        proj.create_project_item(item_id, item_text, item_type, item_created_at, item_modified_at, item_author)
        self.seeker.projects.update_project(proj)
        self.seeker.projects.cursor.execute("SELECT name, owner, state FROM Project WHERE name = '{}' AND owner = '{}'".format(proj_name, proj_owner))
        try:
            name, owner, state = self.seeker.projects.cursor.fetchone()
            self.assertEqual(proj_name, name)
            self.assertEqual(proj_owner, owner)
            self.assertEqual(ProjectSyncState.IDLE, ProjectSyncState[state])
        except (ValueError, IndexError, TypeError):
            self.fail("SELECT não encontrou '{}'".format(proj_name))
        self.seeker.projects.cursor.execute(
            "SELECT id, projectName, projectOwner, type, projectItemModificationId, createdAt, text, author, relevant FROM ProjectItem WHERE projectName = '{}' AND projectOwner = '{}'".format(
                self.proj_name, self.proj_owner))
        try:
            id, project_name, project_owner, type, project_item_modification_id, created_at, text, author, relevant = self.seeker.projects.cursor.fetchall()[0]
            self.assertEqual(item_id, id)
            self.assertEqual(self.proj_owner, project_owner)
            self.assertEqual(self.proj_name, project_name)
            self.assertEqual(item_type, ProjectItemType[type])
            self.assertEqual(item_author, author)
            self.assertEqual(item_created_at, created_at)
            self.assertEqual(item_text, text)
            self.assertEqual(relevant, None)
        except (ValueError, IndexError, TypeError):
            self.fail("SELECT não encontrou '{}'".format(proj_name))

    def test_update_4attr_project(self):
        self.seeker.projects.update_project(self.proj_name, self.proj_owner,
                                            new_name=self.new_proj_name,
                                            new_owner=self.new_proj_owner,
                                            new_relevant=self.new_proj_relevant,
                                            new_state=self.new_proj_state)
        self.seeker.logins.cursor.execute("SELECT name, owner, relevant, state FROM Project WHERE name = '{}' AND owner = '{}'".format(self.proj_name, self.proj_owner))
        with self.assertRaises((ValueError, IndexError, TypeError)):
            _, _, _, _ = self.seeker.projects.cursor.fetchone()
        self.seeker.logins.cursor.execute("SELECT name, owner, relevant, state FROM Project WHERE name = '{}' AND owner = '{}'".format(self.new_proj_name, self.new_proj_owner))
        try:
            name, owner, relevant, state = self.seeker.projects.cursor.fetchone()
            self.assertEqual(name, self.new_proj_name)
            self.assertEqual(owner, self.new_proj_owner)
            self.assertEqual(relevant, self.new_proj_relevant)
            self.assertEqual(ProjectSyncState[state], self.new_proj_state)
        except (ValueError, IndexError, TypeError):
            self.fail("Couldn't find Project with new values! Update didn't work.")

    def test_update_3attr_project(self):
        self.seeker.projects.update_project(self.proj_name, self.proj_owner,
                                            new_relevant=self.new_proj_relevant,
                                            new_name=self.new_proj_name,
                                            new_state=self.new_proj_state)
        self.seeker.logins.cursor.execute("SELECT name, owner, relevant, state FROM Project WHERE name = '{}' AND owner = '{}'".format(self.proj_name, self.proj_owner))
        with self.assertRaises((ValueError, IndexError, TypeError)):
            _, _, _, _ = self.seeker.projects.cursor.fetchone()
        self.seeker.logins.cursor.execute("SELECT name, owner, relevant, state FROM Project WHERE name = '{}' AND owner = '{}'".format(self.new_proj_name, self.proj_owner))
        try:
            name, owner, relevant, state = self.seeker.projects.cursor.fetchone()
            self.assertEqual(name, self.new_proj_name)
            self.assertEqual(owner, self.proj_owner)
            self.assertEqual(relevant, self.new_proj_relevant)
            self.assertEqual(ProjectSyncState[state], self.new_proj_state)
        except (ValueError, IndexError, TypeError):
            self.fail("Couldn't find Project with new values! Update didn't work.")

    def test_update_2attr_project(self):
        self.seeker.projects.update_project(self.proj_name, self.proj_owner,
                                            new_relevant=self.new_proj_relevant,
                                            new_owner=self.new_proj_owner)
        self.seeker.logins.cursor.execute("SELECT name, owner, relevant, state FROM Project WHERE name = '{}' AND owner = '{}'".format(self.proj_name, self.proj_owner))
        with self.assertRaises((ValueError, IndexError, TypeError)):
            _, _, _, _ = self.seeker.projects.cursor.fetchone()
        self.seeker.logins.cursor.execute("SELECT name, owner, relevant, state FROM Project WHERE name = '{}' AND owner = '{}'".format(self.proj_name, self.new_proj_owner))
        try:
            name, owner, relevant, state = self.seeker.projects.cursor.fetchone()
            self.assertEqual(name, self.proj_name)
            self.assertEqual(owner, self.new_proj_owner)
            self.assertEqual(relevant, self.new_proj_relevant)
            self.assertEqual(ProjectSyncState[state], self.proj_state)
        except (ValueError, IndexError, TypeError):
            self.fail("Couldn't find Project with new values! Update didn't work.")

    def test_update_1attr_project(self):
        self.seeker.projects.update_project(self.proj_name, self.proj_owner,
                                            new_state=self.new_proj_state)
        self.seeker.logins.cursor.execute("SELECT name, owner, relevant, state FROM Project WHERE name = '{}' AND owner = '{}'".format(self.proj_name, self.proj_owner))
        try:
            name, owner, relevant, state = self.seeker.projects.cursor.fetchone()
            self.assertEqual(name, self.proj_name)
            self.assertEqual(owner, self.proj_owner)
            self.assertFalse(relevant)
            self.assertEqual(ProjectSyncState[state], self.new_proj_state)
        except (ValueError, IndexError, TypeError):
            self.fail("Couldn't find Project with new values! Update didn't work.")

    def test_delete_project(self):
        self.seeker.logins.cursor.execute("SELECT name, owner, relevant, state FROM Project WHERE name = '{}' AND owner = '{}'".format(self.proj_name, self.proj_owner))
        name, owner, relevant, state = self.seeker.projects.cursor.fetchone()
        self.assertEqual(name, self.proj_name)
        self.assertEqual(owner, self.proj_owner)
        self.assertFalse(relevant)
        self.assertEqual(ProjectSyncState[state], self.proj_state)
        self.seeker.projects.delete_project(self.proj_name, self.proj_owner)
        self.seeker.logins.cursor.execute("SELECT name, owner, relevant, state FROM Project WHERE name = '{}' AND owner = '{}'".format(self.proj_name, self.proj_owner))
        with self.assertRaises((ValueError, IndexError, TypeError)):
            _, _, _, _ = self.seeker.projects.cursor.fetchone()

    def test_get_project(self):
        proj = self.seeker.projects.get_project(self.proj_name, self.proj_owner)
        self.assertEqual(proj.name, self.proj_name)
        self.assertEqual(proj.owner, self.proj_owner)
        self.assertFalse(proj.relevant)
        self.assertEqual(ProjectSyncState[proj.state], self.proj_state)

    def test_get_projects(self):
        name = [self.fake.first_name() for _ in range(50)]
        owner = [re.sub(' ', '', self.fake.name()) for _ in range(50)]
        relevant = [random.random() >= 0.5 for _ in range(50)]
        state = [random.choice(list(ProjectSyncState)) for _ in range(50)]
        for n, o, r, s in zip(name, owner, relevant, state):
            self.seeker.logins.cursor.execute("INSERT INTO Project (name, owner, relevant, state) VALUES ('{}', '{}', '{}', '{}')".format(n, o, int(r), s))
            Connector.db.commit()
        self.seeker.projects.get_projects()
