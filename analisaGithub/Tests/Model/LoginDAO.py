from unittest import TestCase
from faker import Faker
from analisaGithub.Controller.Connector import Connector
from analisaGithub.Controller.Seeker import Seeker
from mysql.connector.cursor_cext import CMySQLCursor as Cursor

from analisaGithub.Model.DataBaseService import DataBaseService
from analisaGithub.Model.Login import Login


class TestLoginDAO(TestCase):
    def setUp(self):
        self.seeker = Seeker()
        self.seeker.establish_connection_db('localhost', "victorgv", "12345", 'mydb')
        self.fake = Faker()
        self.old_username = "fadoa"
        self.new_username = "victor"
        self.old_password = "glauss"
        self.new_password = "veloso"
        self.db = DataBaseService.GITHUB
        self.seeker.logins.cursor.execute("INSERT INTO Login (username, password, dbService) VALUES ('{}', '{}', '{}')".format(self.old_username, self.old_password, self.db))
        Connector.db.commit()

    def tearDown(self):
        import os
        try:
            os.remove("arquivo.dat")
        except FileNotFoundError:
            pass
        self.seeker.logins.cursor.execute("DROP SCHEMA mydb;")
        Connector.db.commit()
        with open('../DER.sql') as f:
            self.seeker.logins.cursor.execute(f.read(), multi=True)

    def test_get_logins(self):
        username = [self.fake.name() for _ in range(50)]
        passwd = [self.fake.phone_number() for _ in range(50)]
        db = ["GITHUB"] * 50
        for u, p, d in zip(username, passwd, db):
            self.seeker.logins.cursor.execute("INSERT INTO Login (username, password, dbService) VALUES ('{}', '{}', '{}')".format(u, p, d))
            Connector.db.commit()
        logins = self.seeker.logins.get_logins()
        for u, p in zip(username, passwd):
            self.assertIn((u, DataBaseService[db[0]], p), [(login.username, login.db_service, login.password) for login in logins])

    def test_select_not_created_login(self):
        self.seeker.logins.cursor.execute(
            "SELECT username, password, dbService FROM Login WHERE username = '{}'".format(self.new_username))
        with self.assertRaises((IndexError, ValueError, TypeError)):
            _, _, _ = self.seeker.logins.cursor.fetchall()[0]

    def test_create_login(self):
        self.seeker.logins.create_login(self.new_username, self.old_password, self.db)
        Connector.db.commit()
        self.seeker.logins.cursor.execute("SELECT username, password, dbService FROM Login WHERE username = '{}'".format(self.new_username))
        try:
            u, p, d = self.seeker.logins.cursor.fetchall()[0]
            self.assertEqual(self.new_username, u)
            self.assertEqual(self.old_password, p)
            self.assertEqual(self.db, DataBaseService[d])
        except (IndexError, ValueError, TypeError):
            self.fail("SELECT não encontrou {}".format(self.new_username))

    def test_update_1attr_login(self):
        self.seeker.logins.update_login(self.old_username, self.db, new_username=self.new_username)
        Connector.db.commit()
        self.seeker.logins.cursor.execute("SELECT username, password, dbService FROM Login WHERE username = '{}' AND dbService = '{}'".format(self.old_username, self.db))
        with self.assertRaises((IndexError, ValueError, TypeError)):
            _, _, _ = self.seeker.logins.cursor.fetchone()
        self.seeker.logins.cursor.execute("SELECT username, password, dbService FROM Login WHERE username = '{}' AND dbService = '{}'".format(self.new_username, self.db))
        try:
            u, p, d = self.seeker.logins.cursor.fetchone()
            self.assertNotEqual(self.old_username, u)
            self.assertEqual(self.new_username, u)
            self.assertEqual(self.old_password, p)
            self.assertEqual(self.db, DataBaseService[d])
        except (IndexError, ValueError, TypeError):
            self.fail("Couldn't find Login with new values! Update didn't work.")

    def test_update_2attr_login(self):
        self.seeker.logins.update_login(self.old_username, self.db, new_username=self.new_username,
                                        new_password=self.new_password)
        Connector.db.commit()
        self.seeker.logins.cursor.execute("SELECT username, password, dbService FROM Login WHERE username = '{}' AND dbService = '{}'".format(self.old_username, self.db))
        with self.assertRaises((IndexError, ValueError, TypeError)):
            _, _, _ = self.seeker.logins.cursor.fetchone()
        self.seeker.logins.cursor.execute("SELECT username, password, dbService FROM Login WHERE username = '{}' AND dbService = '{}'".format(self.new_username, self.db))
        try:
            u, p, d = self.seeker.logins.cursor.fetchone()
            self.assertNotEqual(self.old_username, u)
            self.assertEqual(self.new_username, u)
            self.assertNotEqual(self.old_password, p)
            self.assertEqual(self.new_password, p)
            self.assertEqual(self.db, DataBaseService[d])
        except (IndexError, ValueError, TypeError):
            self.fail("Couldn't find Login with new values! Update didn't work.")

    def test_update_3attr_login(self):
        self.seeker.logins.update_login(self.old_username, self.db, new_username=self.new_username, new_password=self.new_password, new_db=self.db)
        Connector.db.commit()
        self.seeker.logins.cursor.execute("SELECT username, password, dbService FROM Login WHERE username = '{}' AND dbService = '{}'".format(self.old_username, self.db))
        with self.assertRaises((IndexError, ValueError, TypeError)):
            _, _, _ = self.seeker.logins.cursor.fetchone()
        self.seeker.logins.cursor.execute("SELECT username, password, dbService FROM Login WHERE username = '{}' AND dbService = '{}'".format(self.new_username, self.db))
        try:
            u, p, d = self.seeker.logins.cursor.fetchone()
            self.assertNotEqual(self.old_username, u)
            self.assertEqual(self.new_username, u)
            self.assertNotEqual(self.old_password, p)
            self.assertEqual(self.new_password, p)
            self.assertEqual(self.db, DataBaseService[d])
        except (IndexError, ValueError, TypeError):
            self.fail("Couldn't find Login with new values! Update didn't work.")

    def test_get_login(self):
        login = self.seeker.logins.get_login(self.old_username, self.db)
        self.assertTrue(isinstance(login, Login))
        self.assertEqual(login.username, self.old_username)
        self.assertEqual(login.password, self.old_password)
        self.assertEqual(DataBaseService[login.db_service], self.db)

    def test_delete_login(self):
        self.seeker.logins.delete_login(self.old_username, self.db)
        Connector.db.commit()
        self.seeker.logins.cursor.execute(
            "SELECT username, password, dbService FROM Login WHERE username = '{}'".format(self.old_username))
        with self.assertRaises((IndexError, ValueError, TypeError)):
            _, _, _ = self.seeker.logins.cursor.fetchone()
