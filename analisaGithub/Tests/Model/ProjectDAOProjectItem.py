import re
import sys
from datetime import date
from time import sleep
from unittest import TestCase

import mysql.connector.errors
from faker import Faker
from analisaGithub.Controller.Connector import Connector
from analisaGithub.Controller.Seeker import Seeker
import random
from analisaGithub.Model.DataBaseService import DataBaseService
from analisaGithub.Model.Project import Project
from analisaGithub.Model.ProjectItem import ProjectItem
from analisaGithub.Model.ProjectItemType import ProjectItemType


class TestProjectDAO(TestCase):
    def setUp(self):
        self.proj_name = 'BusinessSysMan'
        self.proj_owner = 'primary157'
        self.username = "victorgv"
        self.passwd = "12345"
        self.db = 'mydb'
        self.fake = Faker()
        self.seeker = Seeker()
        self.seeker.establish_connection_db('localhost', self.username, self.passwd, self.db)
        self.username = "fadoa"
        self.password = "glauss"
        self.db = DataBaseService.GITHUB
        self.seeker.logins.cursor.execute("INSERT INTO Login (username, password, dbService) VALUES ('{}', '{}', '{}')".format(self.username, self.password, self.db))
        Connector.db.commit()
        self.seeker.logins.cursor.execute("INSERT INTO Project (name, owner) VALUES ('{}', '{}')".format(self.proj_name, self.proj_owner))
        Connector.db.commit()
        self.type = random.choice(list(ProjectItemType))
        self.text = self.fake.sentence(nb_words=30, variable_nb_words=True, ext_word_list=None)
        self.item_id = str(random.randint(0, sys.maxsize - 1))
        self.author = re.sub(' ', '', self.fake.name()) + "@example.com"
        self.created_at = self.fake.past_date(start_date="-900d", tzinfo=None)
        self.modified_at = [self.fake.past_date(start_date=str((self.created_at - date.today()).days) + 'd', tzinfo=None)]
        proj = self.seeker.projects.get_project(self.proj_name, self.proj_owner)
        self.seeker.projects._add_project_item(proj, self.item_id, text=self.text, type=self.type, created_at=self.created_at, modified_at=self.modified_at, author=self.author)

    def tearDown(self):
        import os
        try:
            os.remove("arquivo.dat")
        except FileNotFoundError:
            pass
        self.seeker.logins.cursor.execute("DROP SCHEMA mydb;")
        Connector.db.commit()
        with open('../DER.sql') as f:
            self.seeker.logins.cursor.execute(f.read(), multi=True)
        sleep(6)

    def test_add_project_item(self):
        item_type = random.choice(list(ProjectItemType))
        item_text = self.fake.sentence(nb_words=30, variable_nb_words=True, ext_word_list=None)
        item_id = str(random.randint(0, sys.maxsize - 1))
        item_created_at = self.fake.past_date(start_date="-900d", tzinfo=None)
        item_modified_at = [self.fake.past_date(start_date=str((item_created_at - date.today()).days) + 'd', tzinfo=None)]
        item_author = re.sub(' ', '', self.fake.name()) + "@example.com"
        proj = self.seeker.projects.get_project(self.proj_name, self.proj_owner)
        self.seeker.projects._add_project_item(proj, item_id, item_text, item_type, item_created_at, item_modified_at, item_author)
        self.seeker.projects.cursor.execute(
            "SELECT id, projectName, projectOwner, type, projectItemModificationId, createdAt, text, author, relevant FROM ProjectItem WHERE id = '{}' AND projectName = '{}' AND projectOwner = '{}'".format(
                item_id, self.proj_name, self.proj_owner))
        try:
            id, project_name, project_owner, type, project_item_modification_id, created_at, text, author, relevant = self.seeker.projects.cursor.fetchone()
            self.assertEqual(item_id, id)
            self.assertEqual(self.proj_owner, project_owner)
            self.assertEqual(self.proj_name, project_name)
            self.assertEqual(item_type, ProjectItemType[type])
            self.assertEqual(item_author, author)
            self.assertEqual(item_created_at, created_at)
            self.assertEqual(item_text, text)
            self.assertEqual(relevant, None)
        except (ValueError, IndexError, TypeError):
            self.fail("SELECT não encontrou item '{}'".format(item_id))
        self.seeker.projects.cursor.execute("SELECT date FROM ModificationDate WHERE projectItemModificationId = '{}'".format(project_item_modification_id))
        try:
            dates = self.seeker.projects.cursor.fetchall()
            for d in dates:
                self.assertIn(d[0], item_modified_at)
        except (ValueError, IndexError, TypeError):
            self.fail("SELECT não encontrou data '{}'".format(project_item_modification_id))

    def test_get_project_item(self):
        item = self.seeker.projects._get_project_item(Project(self.proj_name, self.proj_owner), self.item_id)
        self.assertTrue(isinstance(item, ProjectItem))
        self.assertEqual(item.type, self.type)
        self.assertEqual(item.text, self.text.lower())  # ProjectItem text is lowered at constructor
        for d in self.modified_at:
            self.assertIn(d, item.modified_at)
        self.assertEqual(item.created_at, self.created_at)
        self.assertEqual(item.relevant, None)
        self.assertEqual(item.author, self.author)
        self.assertEqual(item.id, self.item_id)

    def test_get_project_items(self):
        items = self.seeker.projects._get_project_items(Project(self.proj_name, self.proj_owner))
        self.assertTrue(isinstance(items, list) and all(isinstance(item, ProjectItem) for item in items))
        for item in items:
            self.assertEqual(item.id, self.item_id)
            self.assertEqual(item.type, self.type)

    def test_update_project_item(self):
        new_relevant = random.random() >= 0.5
        new_modified_at = [date.today()]
        new_text = self.fake.sentence(nb_words=30, variable_nb_words=True, ext_word_list=None)
        new_type = random.choice(list(ProjectItemType))
        new_id = str(random.randint(0, sys.maxsize - 1))
        new_author = re.sub(' ', '', self.fake.name()) + "@example.com"
        self.seeker.projects.cursor.execute(
            "SELECT projectItemModificationId FROM ProjectItem WHERE id = '{}' AND projectName = '{}' AND projectOwner = '{}'".format(self.item_id, self.proj_name, self.proj_owner))
        mod_id = self.seeker.projects.cursor.fetchone()[0]
        self.seeker.projects._update_project_item(Project(self.proj_name, self.proj_owner), self.item_id,
                                                  new_text=new_text,
                                                  new_id=new_id,
                                                  new_type=new_type,
                                                  new_relevant=new_relevant,
                                                  new_modified_at=new_modified_at,
                                                  new_author=new_author)
        self.seeker.projects.cursor.execute(
            "SELECT id, projectName, projectOwner, type, projectItemModificationId, createdAt, text, author FROM ProjectItem WHERE id = '{}' AND projectName = '{}' AND projectOwner = '{}'".format(
                self.item_id, self.proj_name, self.proj_owner))
        self.assertTrue(self.seeker.projects.cursor.fetchone() is None)
        self.seeker.projects.cursor.execute(
            "SELECT id, projectName, projectOwner, type, projectItemModificationId, createdAt, text, author FROM ProjectItem WHERE id = '{}' AND projectName = '{}' AND projectOwner = '{}'".format(
                new_id, self.proj_name, self.proj_owner))
        try:
            id, project_name, project_owner, type, project_item_modification_id, created_at, text, author = self.seeker.projects.cursor.fetchone()
        except (ValueError, IndexError, TypeError):
            self.fail("Couldn't find Item with new values! Update didn't work")
        self.assertEqual(id, new_id)
        self.assertEqual(project_name, self.proj_name)
        self.assertEqual(project_owner, self.proj_owner)
        self.assertEqual(ProjectItemType[type], new_type)
        self.assertEqual(text, new_text)
        self.assertEqual(author, new_author)
        self.assertEqual(project_item_modification_id, mod_id)
        self.seeker.projects.cursor.execute("SELECT date FROM ModificationDate WHERE projectItemModificationId = '{}'".format(mod_id))
        try:
            dates = [d[0] for d in self.seeker.projects.cursor.fetchall()]
            self.assertListEqual(dates, new_modified_at)
        except (ValueError, IndexError, TypeError):
            self.fail("Couldn't find ModificationDate")

    def test_rem_project_item(self):
        self.seeker.projects.cursor.execute(
            "SELECT projectItemModificationId FROM ProjectItem WHERE id = '{}' AND projectName = '{}' AND projectOwner = '{}'".format(self.item_id, self.proj_name, self.proj_owner))
        try:
            project_item_modification_id = self.seeker.projects.cursor.fetchone()
        except (ValueError, IndexError, TypeError):
            self.fail("Couldn't find item")
        self.seeker.projects._rem_project_item(Project(self.proj_name, self.proj_owner), self.item_id)
        self.seeker.projects.cursor.execute(
            "SELECT id, type, projectItemModificationId, createdAt, text, author, relevant FROM ProjectItem WHERE id = '{}' AND projectName = '{}' AND projectOwner = '{}'".format(self.item_id,
                                                                                                                                                                                   self.proj_name,
                                                                                                                                                                                   self.proj_owner))
        self.assertTrue(self.seeker.projects.cursor.fetchone() is None)
        self.seeker.projects.cursor.execute("SELECT date FROM ModificationDate WHERE projectItemModificationId = '{}'".format(project_item_modification_id))
        self.assertEqual(len(self.seeker.projects.cursor.fetchall()), 0)
        self.seeker.projects.cursor.execute("SELECT * FROM ProjectItemModification WHERE id = '{}'".format(self.item_id))
        self.assertTrue(self.seeker.projects.cursor.fetchone() is None)

    def test_add_correlation(self):
        type = random.choice(list(ProjectItemType))
        text = self.fake.sentence(nb_words=30, variable_nb_words=True, ext_word_list=None)
        id = str(random.randint(0, sys.maxsize - 1))
        author = re.sub(' ', '', self.fake.name()) + "@example.com"
        created_at = self.fake.past_date(start_date="-900d", tzinfo=None)
        modified_at = [self.fake.past_date(start_date=str((self.created_at - date.today()).days) + 'd', tzinfo=None)]
        self.seeker.projects._add_project_item(Project(self.proj_name, self.proj_owner), id, text, type, created_at, modified_at, author)
        self.seeker.projects.add_correlation(self.proj_name, self.proj_owner, self.item_id, id)
        self.seeker.projects.cursor.execute(
            "SELECT itemId, correlatedId FROM ProjectItemCorrelation WHERE itemProjectName = '{1}' AND itemProjectOwner = '{2}' AND (itemId = '{0}' OR correlatedId = '{0}')".format(
                self.item_id, self.proj_name, self.proj_owner))
        try:
            correlateds = self.seeker.projects.cursor.fetchone()
            self.assertIn(id, correlateds)
        except (ValueError, IndexError, TypeError):
            self.fail("Couldn't find correlation")

    def test_add_comment(self):
        comment_text = self.fake.sentence(nb_words=30, variable_nb_words=True, ext_word_list=None)
        comment_id = str(random.randint(0, sys.maxsize - 1))
        comment_created_at = self.fake.past_date(start_date="-900d", tzinfo=None)
        comment_modified_at = [self.fake.past_date(start_date=str((self.created_at - date.today()).days) + 'd', tzinfo=None)]
        comment_author = re.sub(' ', '', self.fake.name()) + "@example.com"
        self.seeker.projects.add_comment(self.proj_name, self.proj_owner, self.item_id, comment_id, text=comment_text, created_at=comment_created_at, modified_at=comment_modified_at,
                                         author=comment_author)
        Connector.db.commit()
        self.seeker.projects.cursor.execute(
            "SELECT id, text, relevant, createdAt, author, projectItemModificationId FROM ProjectItemComment WHERE projectItemId = '{}' AND projectName = '{}' AND projectOwner = '{}'".format(
                self.item_id, self.proj_name, self.proj_owner))
        try:
            id, text, relevant, created_at, author, project_item_modification_id = self.seeker.projects.cursor.fetchone()
            self.assertEqual(id, comment_id)
            self.assertEqual(text, comment_text)
            self.assertEqual(relevant, None)
            self.assertEqual(created_at, comment_created_at)
            self.assertEqual(author, comment_author)
        except (ValueError, IndexError, TypeError):
            self.fail("Couldn't find correlation")
        self.seeker.projects.cursor.execute("SELECT date FROM ModificationDate WHERE projectItemModificationId = '{}'".format(project_item_modification_id))
        try:
            dates = [d[0] for d in self.seeker.projects.cursor.fetchall()]
            self.assertListEqual(dates, comment_modified_at)
        except (ValueError, IndexError, TypeError):
            self.fail("Couldn't find correlation")

    def test_get_correlateds(self):
        type = random.choice(list(ProjectItemType))
        text = self.fake.sentence(nb_words=30, variable_nb_words=True, ext_word_list=None)
        id = str(random.randint(0, sys.maxsize - 1))
        author = re.sub(' ', '', self.fake.name()) + "@example.com"
        created_at = self.fake.past_date(start_date="-900d", tzinfo=None)
        modified_at = [self.fake.past_date(start_date=str((self.created_at - date.today()).days) + 'd', tzinfo=None)]
        self.seeker.projects._add_project_item(Project(self.proj_name, self.proj_owner), id, text, type, created_at, modified_at, author)
        self.seeker.projects.cursor.execute(
            "INSERT INTO ProjectItemCorrelation (itemId, itemProjectName, itemProjectOwner, correlatedId, correlatedProjectName, correlatedProjectOwner) VALUES ('{0}', '{1}','{2}', '{3}','{1}', '{2}')".format(
                self.item_id, self.proj_name, self.proj_owner, id))
        correlateds = self.seeker.projects.get_correlateds(self.proj_name, self.proj_owner, self.item_id)
        self.assertEqual(correlateds[0].type, type)
        self.assertEqual(correlateds[0].text, text.lower())  # ProjectItem text is lowered at constructor
        self.assertEqual(correlateds[0].id, id)
        self.assertEqual(correlateds[0].author, author)
        self.assertEqual(correlateds[0].created_at, created_at)
        self.assertEqual(correlateds[0].modified_at, modified_at)

    def test_get_comments(self):
        comment_text = self.fake.sentence(nb_words=30, variable_nb_words=True, ext_word_list=None)
        comment_id = str(random.randint(0, sys.maxsize - 1))
        comment_created_at = self.fake.past_date(start_date="-900d", tzinfo=None)
        comment_modified_at = [self.fake.past_date(start_date=str((self.created_at - date.today()).days) + 'd', tzinfo=None)]
        comment_author = re.sub(' ', '', self.fake.name()) + "@example.com"
        self.seeker.projects.add_comment(self.proj_name, self.proj_owner, self.item_id, comment_id, text=comment_text, created_at=comment_created_at, modified_at=comment_modified_at,
                                         author=comment_author)
        comments = self.seeker.projects.get_comments(self.proj_name, self.proj_owner, self.item_id)
        self.assertEqual(comments[0].id, comment_id)
        self.assertEqual(comments[0].text, comment_text)
        self.assertEqual(comments[0].relevant, None)
        self.assertEqual(comments[0].created_at, comment_created_at)
        self.assertEqual(comments[0].author, comment_author)
        self.assertListEqual(comments[0].modified_at, comment_modified_at)
