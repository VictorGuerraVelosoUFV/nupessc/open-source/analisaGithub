from unittest import TestCase

from faker import Faker

from analisaGithub.Controller.Connector import Connector
from analisaGithub.Controller.Seeker import Seeker
from analisaGithub.Model.DataBaseService import DataBaseService
from analisaGithub.Model.ProjectSyncState import ProjectSyncState


class ProjectLogin(TestCase):
    def setUp(self):
        self.username = "victorgv"
        self.passwd = "12345"
        self.db_name = 'mydb'
        self.seeker = Seeker()
        self.seeker.establish_connection_db('localhost', self.username, self.passwd, self.db_name)
        self.fake = Faker()
        self.proj_name = 'BusinessSysMan'
        self.proj_owner = 'primary157'
        self.proj_state = ProjectSyncState.IDLE
        self.new_proj_relevant = False
        self.new_proj_name = "BSM"
        self.new_proj_owner = "messiahglauss"
        self.new_proj_state = ProjectSyncState.SYNCING
        self.db = DataBaseService.GITHUB
        self.old_username = "fadoa"
        self.old_password = "glauss"
        self.new_username = "victor"
        self.new_password = "veloso"
        self.seeker.logins.cursor.execute("INSERT INTO Login (username, password, dbService) VALUES ('{}', '{}', '{}')".format(self.old_username, self.old_password, self.db))
        Connector.db.commit()
        self.seeker.logins.cursor.execute("INSERT INTO Project (name, owner) VALUES ('{}', '{}')".format(self.proj_name, self.proj_owner))
        Connector.db.commit()

    def tearDown(self):
        import os
        try:
            os.remove("arquivo.dat")
        except FileNotFoundError:
            pass
        self.seeker.logins.cursor.execute("DROP SCHEMA mydb;")
        Connector.db.commit()
        with open('../DER.sql') as f:
            self.seeker.logins.cursor.execute(f.read(), multi=True)

    def test_get_login_projects(self):
        self.seeker.projects.add_project_to_login(self.old_username, self.db, self.proj_name, self.proj_owner)
        projects = self.seeker.logins.get_login_projects(self.old_username, self.db)
        for project in projects:
            self.assertEqual(project.name, self.proj_name)
            self.assertEqual(project.owner, self.proj_owner)
            self.assertFalse(project.relevant)
            self.assertEqual(project.state, ProjectSyncState.IDLE)

    def test_add_project_to_login(self):
        self.seeker.projects.add_project_to_login(self.old_username, self.db, self.proj_name, self.proj_owner)
        self.seeker.logins.cursor.execute("SELECT name, owner, loginUsername, dbService, relevant, state FROM Project WHERE name = '{}' AND owner = '{}'".format(self.proj_name, self.proj_owner))
        name, owner, u, d, relevant, state = self.seeker.projects.cursor.fetchone()
        self.assertEqual(name, self.proj_name)
        self.assertEqual(owner, self.proj_owner)
        self.assertFalse(relevant)
        self.assertEqual(ProjectSyncState[state], self.proj_state)
        self.assertEqual(u, self.old_username)
        self.assertEqual(DataBaseService[d], self.db)
