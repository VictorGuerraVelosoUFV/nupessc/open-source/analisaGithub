import json
import os

from analisaGithub.Model.ProjectItemType import ProjectItemType
from analisaGithub.Model.Project import Project
from analisaGithub.Model.Analysis import Analysis
from analisaGithub.Model.RelevanceHistory import RelevanceHistory


class JSONExporter:
    @staticmethod
    def export_analysis(a, f):
        assert isinstance(a, Analysis)
        assert isinstance(f, str) and not os.path.exists(f)
        a_items = {"ProjectItemType.COMMIT": {
                       "ProjectItemType.COMMIT": a.items[ProjectItemType.COMMIT][ProjectItemType.COMMIT],
                       "ProjectItemType.ISSUE": a.items[ProjectItemType.COMMIT][ProjectItemType.ISSUE],
                       "ProjectItemType.PULL_REQUEST": a.items[ProjectItemType.COMMIT][ProjectItemType.PULL_REQUEST],
                       "ProjectItemType.FILE": a.items[ProjectItemType.COMMIT][ProjectItemType.FILE]},
                   "ProjectItemType.ISSUE": {
                       "ProjectItemType.COMMIT": a.items[ProjectItemType.COMMIT][ProjectItemType.ISSUE],
                       "ProjectItemType.ISSUE": a.items[ProjectItemType.ISSUE][ProjectItemType.ISSUE],
                       "ProjectItemType.PULL_REQUEST": a.items[ProjectItemType.ISSUE][ProjectItemType.PULL_REQUEST],
                       "ProjectItemType.FILE": a.items[ProjectItemType.ISSUE][ProjectItemType.FILE]},
                   "ProjectItemType.PULL_REQUEST": {
                       "ProjectItemType.COMMIT": a.items[ProjectItemType.PULL_REQUEST][ProjectItemType.COMMIT],
                       "ProjectItemType.ISSUE": a.items[ProjectItemType.PULL_REQUEST][ProjectItemType.ISSUE],
                       "ProjectItemType.PULL_REQUEST": a.items[ProjectItemType.PULL_REQUEST][
                           ProjectItemType.PULL_REQUEST],
                       "ProjectItemType.FILE": a.items[ProjectItemType.PULL_REQUEST][ProjectItemType.FILE]},
                   "ProjectItemType.FILE": {
                       "ProjectItemType.COMMIT": a.items[ProjectItemType.FILE][ProjectItemType.COMMIT],
                       "ProjectItemType.ISSUE": a.items[ProjectItemType.FILE][ProjectItemType.ISSUE],
                       "ProjectItemType.PULL_REQUEST": a.items[ProjectItemType.FILE][ProjectItemType.PULL_REQUEST],
                       "ProjectItemType.FILE": a.items[ProjectItemType.FILE][ProjectItemType.FILE]}}
        with open(f, 'w') as file:
            json.dump(a_items, file)

    @staticmethod
    def export_history(h, f):
        assert isinstance(h, RelevanceHistory)
        assert isinstance(f, str) and not os.path.exists(f)
        with open(f, 'w') as file:
            json.dump({'type': str(h.type), 'date': str(h.date), 'quantity': str(h.quantity)}, file)

    @staticmethod
    def export_project_structure(p, f):
        assert isinstance(p, Project)
        assert isinstance(f, str) and not os.path.exists(f)
        proj = {"name": p.name, "items": []}
        for p_i in p.project_items.values():
            proj["items"].append({"id": p_i.id, "type": str(p_i.type), "text": p_i.text, "author": p_i.author})
            for comment in p_i.comment.values():
                proj["items"].append({"id": comment.id, "type": "ProjectItemType.COMMENT", "text": comment.text, "author": p_i.author})
        with open(f, 'w') as file:
            json.dump(proj, file)
