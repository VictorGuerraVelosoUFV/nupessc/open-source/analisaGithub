from analisaGithub.Model.DataBaseService import DataBaseService
from analisaGithub.Model.Login import Login
from github import Github
from mysql import connector


class Connector:
    db = None

    def __init__(self, s):
        assert isinstance(s, DataBaseService)
        self.dataBaseService = s
        self.cvs = None

    def connect(self, login):
        assert isinstance(login, Login)
        if self.dataBaseService == DataBaseService.GITHUB:
            self.cvs = Github(login.username, login.password)
            _ = self.cvs.get_user().get_repos()[0]  # raise BadCredentialsException for bad credentials
        else:
            raise RuntimeError("Not connected yet")

    def disconnect(self):
        del self.cvs

    @classmethod
    def connect_db(cls, host, username, password, db):
        cls.db = connector.connect(
            host=host,
            user=username,
            passwd=password,
            database=db
        )

    @classmethod
    def disconnect_db(cls):
        del cls.db
