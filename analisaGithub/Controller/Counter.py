from analisaGithub.Model.AnalysisParameterType import AnalysisParameterType
from analisaGithub.Model.ProjectDAO import ProjectDAO
from analisaGithub.Model.AnalysisParameter import AnalysisParameter
from analisaGithub.Model.ProjectItem import ProjectItem
from analisaGithub.Model.ProjectItemType import ProjectItemType
from analisaGithub.Model.Project import Project
from analisaGithub.Model.ProjectSyncState import ProjectSyncState


class Counter:
    def __init__(self, p_d=ProjectDAO()):
        assert isinstance(p_d, ProjectDAO)
        self.proj_dao = p_d

    @staticmethod
    def find_analysis_parameter_in_item(p, item, a_p):
        for analysis_parameter in a_p:
            assert isinstance(analysis_parameter, AnalysisParameter)
            for word in analysis_parameter.words:
                if analysis_parameter.type == AnalysisParameterType.TEXT and word in item.text:
                    p.set_project_relevant(item.id)  # create relevance history
                    item.relevant = True
                    return 1
                elif analysis_parameter.type == AnalysisParameterType.EXTENSIONS:
                    if item.type == ProjectItemType.FILE:
                        if word in item.text.split(".")[-1]:
                            p.set_project_relevant(item.id)  # create relevance history
                            item.relevant = True
                            return 1
        return 0

    @staticmethod
    def enumerate_artifact_related_items_from_proj(p, a_p, r_p_t):
        assert isinstance(p, Project)
        counter = 0
        for item in p.project_items.values():
            assert isinstance(item, ProjectItem)
            c = counter
            for comment in item.comment.values():
                counter += Counter.find_analysis_parameter_in_item(p, comment, a_p)
            if counter > c:  # if item has relevant comment then it's relevant too
                p.set_project_relevant(item.id)
                item.relevant = True
                counter += 1
            if not item.relevant and item.type in r_p_t:
                counter += Counter.find_analysis_parameter_in_item(p, item, a_p)
        if counter:
            p.relevant = True

    def enumerate_artifact_related_items(self, a_p, r_p_t):
        assert isinstance(a_p, list) and all(isinstance(parameter, AnalysisParameter) for parameter in a_p)
        assert isinstance(r_p_t, list) and all(isinstance(parameter_type, ProjectItemType) for parameter_type in r_p_t)
        for i in self.proj_dao.get_projects():
            proj = self.proj_dao.get_project(i)  # enumerate its items
            assert isinstance(proj, Project)
            if proj.state != ProjectSyncState.SUCCEEDED:  # avoid not synced projects
                continue
            Counter.enumerate_artifact_related_items_from_proj(proj, a_p, r_p_t)


if __name__ == '__main__':
    from analisaGithub.Controller.Seeker import Seeker
    from analisaGithub.Model.DataBaseService import DataBaseService
    from analisaGithub.Model.Login import Login

    s = Seeker()
    s.logins.create_login('primary157', input(), DataBaseService.GITHUB)
    s.projects.create_project('BusinessSysMan', 'primary157')
    l = s.logins.get_login('primary157')
    assert isinstance(l, Login)
    l.add_project(s.projects.get_project('BusinessSysMan'))
    s.establish_connection_cvs()
    s.seek_projects()
    c = Counter(s.projects)
    c.enumerate_artifact_related_items([AnalysisParameter('diagram', AnalysisParameterType.TEXT)],
                                       [ProjectItemType.ISSUE, ProjectItemType.COMMIT, ProjectItemType.FILE,
                                        ProjectItemType.PULL_REQUEST, ProjectItemType.COMMENT])
    for project in (s.projects.get_project(name) for name in s.projects.get_projects()):
        for history in project.history:
            print('''
Type: {}
Date: {}
Quantity: {}
            '''.format(history.type, history.date, history.quantity))
    print(input())
