from analisaGithub.Model.Project import Project
from github import GithubException

from analisaGithub.Controller.Connector import Connector
from analisaGithub.Controller.GithubRequestInterface import GithubRequestInterface
from analisaGithub.Model.DataBaseService import DataBaseService
from analisaGithub.Model.ProjectDAO import ProjectDAO
from analisaGithub.Model.LoginDAO import LoginDAO
from analisaGithub.Model.ProjectItem import ProjectItem
from analisaGithub.Model.ProjectItemComment import ProjectItemComment
from analisaGithub.Model.ProjectSyncState import ProjectSyncState


class Seeker:
    def __init__(self, *, login_dao=None, project_dao=None):
        if login_dao is None:
            login_dao = LoginDAO()
        if project_dao is None:
            project_dao = ProjectDAO(login_dao=login_dao)
        self.logins = login_dao
        self.projects = project_dao
        self.connection = []
        self.interface = GithubRequestInterface()

    def seek_projects(self):
        if len(self.connection) == 0 and self.logins.cursor is not None and self.projects.cursor is not None:
            raise RuntimeError("Not connected yet")
        else:
            for connection in self.connection:
                login = self.logins.get_login(self.interface.get_user(connection), DataBaseService.GITHUB)
                for project in self.logins.get_login_projects(login.username, login.db_service):
                    p = self.interface.get_project(project.name, connection)
                    login.projects.append(self.projects.create_project(p["name"], p["owner"]))
                    self.projects.add_project_to_login(login.username, login.db_service, p["name"], p["owner"])
                    try:
                        self.seek_items(self.projects.update_project(login.projects[-1].name, login.projects[-1].owner, new_state=ProjectSyncState.SYNCING), connection)
                    except GithubException:
                        self.projects.update_project(login.projects[-1].name, login.projects[-1].owner, new_state=ProjectSyncState.FAILED)
                    else:
                        self.projects.update_project(login.projects[-1].name, login.projects[-1].owner, new_state=ProjectSyncState.SUCCEEDED)

    def seek_items(self, project, connection):
        assert isinstance(project, Project)
        assert isinstance(connection, Connector)
        for item in self.interface.get_project_items(project.get_url(), connection):
            i = project.create_project_item(item["id"], item["text"], item["type"], item["created_at"], item["modified_at"], item["author"])
            assert isinstance(i, ProjectItem)
            i.created_at = item["created_at"]
            i.modified_at = item["modified_at"]
            self.seek_comments(project, i, connection)
            self.seek_correlateds(project, i, connection)

    def seek_correlateds(self, project, item, connection):
        assert isinstance(project, Project)
        assert isinstance(item, ProjectItem)
        assert isinstance(connection, Connector)
        # seek correlateds from cvs
        # create correlation with DAO
        '''
        for correlated in item["correlated"]:
            item.add_correlated(ProjectItem(correlated[1], str(correlated[0])))
        '''

    def seek_comments(self, project, item, connection):
        assert isinstance(project, Project)
        assert isinstance(item, ProjectItem)
        assert isinstance(connection, Connector)
        # seek comments from cvs
        # create comment with DAO
        '''
        for comment in item["comments"]:
            c = i.add_comment(ProjectItemComment(comment["id"], comment["text"], comment["author"]))
            c.created_at = comment["created_at"]
            c.modified_at = comment["modified_at"]
        '''

    def establish_connection_db(self, host, username, password, db):
        Connector.connect_db(host, username, password, db)
        cursor = Connector.db.cursor()
        self.logins.set_cursor(cursor)
        self.projects.set_cursor(cursor)

    def establish_connection_cvs(self):
        for login in self.logins.get_logins():
            self.connection.append(Connector(login.db_service))
            self.connection[-1].connect(login)
