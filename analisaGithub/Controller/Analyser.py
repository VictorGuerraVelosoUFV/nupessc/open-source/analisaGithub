'''
Class whose goal is transform dumb correlation in connected items, detect new correlations inside texts and
generate analysis
'''
from analisaGithub.Model.Analysis import Analysis
from analisaGithub.Model.AnalysisHistory import AnalysisHistory
from analisaGithub.Model.Project import Project
from analisaGithub.Model.ProjectDAO import ProjectDAO
from analisaGithub.Model.ProjectItem import ProjectItem
from analisaGithub.Model.ProjectItemType import ProjectItemType


class Analyser:
    def __init__(self, proj_dao):
        assert isinstance(proj_dao, ProjectDAO)
        self._proj_dao = proj_dao
        self._analysis = {}

    @property
    def analysis(self):
        return self._analysis

    @analysis.setter
    def analysis(self, value):
        assert isinstance(value, dict) and all(isinstance(v, Analysis) for v in value.values())
        self._analysis = value

    @staticmethod
    def correlate_items(p):
        assert isinstance(p, Project)
        if p.relevant:
            for p_i in p.project_items.values():
                if p_i.type == ProjectItemType.PULL_REQUEST or p_i.type == ProjectItemType.COMMIT:  # pq n issue?
                    for correlated in Analyser.find_correlation_in_text(p_i.text):
                        p_i.correlated[correlated] = p.project_items[correlated]
                        p_i.correlated[correlated].correlated[p_i.id] = p_i
                    for comment in p_i.comment.values():
                        for correlated in Analyser.find_correlation_in_text(comment.text):
                            p_i.correlated[correlated] = p.project_items[correlated]
                            p_i.correlated[correlated].correlated[p_i.id] = p_i

    @staticmethod
    def find_correlation_in_text(text):
        idx = 0
        correlations = []
        while idx < len(text):
            idx = text.find('#', (idx + 1))
            if idx == -1:
                break
            idx += 1
            for i in range(idx, len(text) + 1):
                try:
                    _ = int(text[i])
                except ValueError:  # not number
                    correlations.append(text[idx:i])
                    idx = i
                    break
        return correlations

    @staticmethod
    def group_correlated_items(p, a, r_a):
        assert isinstance(p, Project)
        assert isinstance(a, Analysis)
        assert isinstance(r_a, Analysis)
        if p.relevant:
            for p_i in p.project_items.values():
                assert isinstance(p_i, ProjectItem)
                for c_i in p_i.correlated.keys():
                    # del p_i.correlated[c_i] # Não necessariamente correlacionado é clone / deixar garbage colector
                    p_i.correlated[c_i] = p.project_items[c_i]  # Aponta para o original
                    a[p_i.type][p_i.correlated[c_i].type] += 1
                    p.project_items[c_i].correlated[p_i.id] = p_i  # Correlação reversa
                    a[p_i.correlated[c_i].type][p_i.type] += 1
                    if p_i.relevant or p_i.correlated[c_i].relevant:  # increment relevant analysis
                        h = r_a.add_history(p_i.created_at)  # store related date
                        h.add_author(p_i.author)  # store related author
                        h.inc_mensions()
                        for date in p_i.modified_at:
                            h = r_a.add_history(date)
                            h.add_author(p_i.author)
                            h.inc_mensions()
                        r_a[p_i.type][p_i.correlated[c_i].type] += 1
                        r_a[p_i.correlated[c_i].type][p_i.type] += 1

    def generate_analysis(self):  # for each project
        for p_n in self._proj_dao.get_projects():
            p = self._proj_dao.get_project(p_n)
            if p.relevant:
                self._analysis[p.name] = Analysis(p.name)
                self._analysis[p.name + ".r"] = Analysis(p.name)  # relevant only analysis
                Analyser.correlate_items(p)
                Analyser.group_correlated_items(p, self._analysis[p.name], self._analysis[p.name + ".r"])

    @property
    def proj_dao(self):
        return self._proj_dao


if __name__ == '__main__':
    from analisaGithub.Controller.Seeker import Seeker
    from analisaGithub.Controller.Counter import Counter
    from analisaGithub.Model.DataBaseService import DataBaseService
    from analisaGithub.Model.Login import Login
    from analisaGithub.Model.AnalysisParameter import AnalysisParameter
    from analisaGithub.Model.AnalysisParameterType import AnalysisParameterType

    s = Seeker()
    s.logins.create_login('primary157', input(), DataBaseService.GITHUB)
    s.projects.create_project('BusinessSysMan', 'primary157')
    l = s.logins.get_login('primary157')
    assert isinstance(l, Login)
    l.add_project(s.projects.get_project('BusinessSysMan'))
    s.establish_connection_cvs()
    s.seek_projects()
    c = Counter(s.projects)
    c.enumerate_artifact_related_items([AnalysisParameter('diagram', AnalysisParameterType.TEXT)],
                                       [ProjectItemType.ISSUE, ProjectItemType.COMMIT, ProjectItemType.FILE,
                                        ProjectItemType.PULL_REQUEST])
    for project in (s.projects.get_project(name) for name in s.projects.get_projects()):
        for history in project.history:
            print('''
Type: {}
Date: {}
Quantity: {}
            '''.format(history.type, history.date, history.quantity))
    a = Analyser(c.proj_dao)
    a.generate_analysis()
    for i, j in zip(a.analysis.keys(), a.analysis.values()):
        print('''
==================================================================
{:^66}
==================================================================
{}'''
              .format(i, j))

    print(input())
