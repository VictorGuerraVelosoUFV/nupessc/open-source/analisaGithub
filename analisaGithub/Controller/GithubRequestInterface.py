from datetime import datetime

from github.Commit import Commit
from github.Issue import Issue
from github.PaginatedList import PaginatedList
from github.PullRequest import PullRequest

from analisaGithub.Controller.Connector import Connector
from github.Repository import Repository

from github import GithubException
from analisaGithub.Model.ProjectItemType import ProjectItemType

'''
Adapter class that translates GithubObjects to ProjectItems
Responsible for getting title, owner, text, comments text, type of projectItems and 
its strong correlations (implemented by API)
'''


class GithubRequestInterface():
    def _get_github_repo(self, project_path, connection):  # TODO: Tratar o caso de não achar!!
        assert isinstance(project_path, str)
        assert isinstance(connection, Connector)
        repo = connection.cvs.search_repositories(project_path)
        if isinstance(repo, PaginatedList):
            repo = repo[0]
        return repo

    def get_project(self, project_path, connection):
        assert isinstance(project_path, str)
        repo = self._get_github_repo(project_path, connection)
        return {
            "name": repo.name,
            "owner": repo.owner.login
        }

    def get_project_items(self, project_path, connection):
        repo = self._get_github_repo(project_path, connection)
        if repo.has_issues:
            gets = [self.get_commits_and_files, self.get_issues, self.get_pullrequests]
        else:
            gets = [self.get_commits_and_files, self.get_pullrequests]

        for get_item_of_specific_type in gets:
            try:
                for data in get_item_of_specific_type(repo):
                    yield data
            except GithubException as e:
                if e.data["message"] != "Git Repository is empty.":  # empty projects are not problems
                    raise e

    def get_issues(self, repo):
        assert isinstance(repo, Repository)
        issues = repo.get_issues(state="all")
        project_item = {
            "text": "",
            "id": "",
            "author": "",
            "created_at": datetime.min,
            "modified_at": [],
            "type": ProjectItemType.ISSUE,
            "correlated": [],
            "comments": [],
        }
        for issue in issues:  # percorrer issues do repositorio
            project_item["id"] = str(issue.number)
            project_item["author"] = issue.user.email  # para garantir que seja unico
            project_item["text"] = str(issue.title) + str(issue.body) + str(issue.id)
            project_item["created_at"] = issue.created_at
            project_item["modified_at"] = [issue.closed_at]
            project_item["comments"] = self.get_comments(issue)
            yield project_item

    def _get_file(self, file):
        return {
            "text": file.filename,
            "id": file.sha,
            "author": "",
            "created_at": None,
            "modified_at": [],
            "type": ProjectItemType.FILE,
            "correlated": [],
            "comments": [],
        }

    def get_commits_and_files(self, repo):
        assert isinstance(repo, Repository)
        commits = repo.get_commits()
        project_item = {
            "text": "",
            "id": "",
            "author": "",
            "created_at": datetime.min,
            "modified_at": [],
            "type": ProjectItemType.COMMIT,
            "correlated": [],
            "comments": [],
        }
        for commit in commits:  # percorrer commits do repositorio
            project_item["id"] = commit.commit.sha
            project_item["text"] = commit.commit.message
            project_item["correlated"].clear()
            project_item["author"] = commit.author.email  # para garantir que seja unico
            project_item["created_at"] = commit.commit.author.date
            project_item["modified_at"] = [commit.commit.author.date]
            for file in commit.files:
                f = self._get_file(file)
                f["modified_at"].append(project_item["created_at"])
                f["created_at"] = min(f["modified_at"])  # Recursos Tecnicos para se obter data de criacao
                f["author"] = project_item["author"]
                project_item["correlated"].append((f["id"], f["type"]))
                yield f  # todos arquivos são adicionados por commits
            yield project_item

    def get_pullrequests(self, repo):
        assert isinstance(repo, Repository)
        pulls = repo.get_pulls(state="all")
        project_item = {
            "text": "",
            "id": "",
            "author": "",
            "created_at": datetime.min,
            "modified_at": [],
            "type": ProjectItemType.PULL_REQUEST,
            "correlated": [],
            "comments": [],
        }
        for pull in pulls:  # percorrer PR do repositorio
            project_item["id"] = str(pull.number)
            project_item["author"] = pull.user.email  # para garantir que seja unico
            project_item["text"] = str(pull.title) + str(pull.body) + str(pull.id)
            project_item["correlated"].clear()
            project_item["created_at"] = pull.created_at
            project_item["modified_at"] = [pull.closed_at]
            project_item["comments"] = self.get_comments(pull)
            for commit in pull.get_commits():
                project_item["correlated"].append((commit.sha, ProjectItemType.COMMIT))
            yield project_item

    def get_user(self, connection):
        assert isinstance(connection, Connector)
        return connection.cvs.get_user().login

    def get_comments(self, obj):
        assert any((isinstance(obj, Issue), isinstance(obj, PullRequest)))
        return [{
            "text": comment.body,
            "id": str(comment.id),
            "author": comment.user.email,
            "created_at": comment.created_at,
            "modified_at": [comment.updated_at],
        } for comment in obj.get_comments()]
