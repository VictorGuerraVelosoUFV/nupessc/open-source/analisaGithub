from analisaGithub.Controller.ExportingType import ExportingType
from analisaGithub.Controller.JSONExporter import JSONExporter
from analisaGithub.Controller.Analyser import Analyser
from analisaGithub.Controller.ReportExporter import ReportExporter
from analisaGithub.Model.ProjectItemType import ProjectItemType


class Recorder:
    def __init__(self, a):
        assert isinstance(a, Analyser)
        self._analyser = a
        self._exporters = {}

    def record(self, tp):
        assert isinstance(tp, ExportingType)
        try:
            _ = self._exporters[tp]
        except KeyError:
            if tp == ExportingType.JSON:
                self._exporters[tp] = JSONExporter()
            elif tp == ExportingType.PDF:
                self._exporters[tp] = ReportExporter()
        if len(self._analyser.proj_dao.project.items()):
            for analysis in self._analyser.analysis.values():
                if any(i.values() for i in analysis.items.values()):
                    for idx, a in enumerate(self._analyser.analysis.values()):
                        self._exporters[tp].export_analysis(a, "ateste{}.json".format(idx))
                    return
            if any(len(self._analyser.proj_dao.get_project(name).history) for name in
                   self._analyser.proj_dao.get_projects()):  # already counted
                for idx, a in enumerate(self._analyser.proj_dao.get_projects()):
                    for h_idx, h in enumerate(self._analyser.proj_dao.get_project(a).history):
                        self._exporters[tp].export_history(h, "cteste{}{}.json".format(idx, h_idx))
            else:  # just seeked
                for idx, a in enumerate(self._analyser.proj_dao.get_projects()):
                    self._exporters[tp].export_project_structure(self._analyser.proj_dao.get_project(a),
                                                                 "steste{}.json".format(idx))
        else:  # not even seeked
            raise RuntimeError("Before recording you must use at least one of the other Controllers: Seeker, Counter, Analyser")
