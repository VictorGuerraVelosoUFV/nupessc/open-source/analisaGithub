from enum import Enum


class ExportingType(Enum):
    JSON = 1
    PDF = 2
