#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup
from setuptools.command.install import install as _install


class Install(_install):
    def run(self):
        _install.do_egg_install(self)
        import nltk
        nltk.download("wordnet")


try:
    from pyqt_distutils.build_ui import build_ui

    cmdclass = {'install': Install, 'build_ui': build_ui}
except ImportError:
    cmdclass = {'install': Install}

setup(
    name='analisaGithub',
    version='1.0',
    packages=['analisaGithub', 'analisaGithub.UI', 'analisaGithub.View', 'analisaGithub.Model',
              'analisaGithub.Controller', 'analisaGithub.Tests.UI',
              'analisaGithub.Tests.Model', 'analisaGithub.Tests.Controller', 'analisaGithub.Tests'],
    url='',
    license='GPL',
    author='victorgv',
    author_email='victorgvbh@gmail.com',
    description='',
    python_requires=">=3.4",
    test_suite="analisaGithub.Tests.AllTests.all_tests_suite",
    cmdclass=cmdclass,
    install_requires=[
        "pylatex",
        "PyQt5",
        "PyGithubCached",
        "nltk", # https://stackoverflow.com/questions/26799894/installing-nltk-data-in-setup-py-script
        "numpy",
        "mysql-connector-python", 'faker'
    ],
    dependency_links=['git+https://github.com/primary157/PyGithubCached.git#egg=PyGithubCached-1.35'],
)
