import socket

from github import Github, GithubException
from nltk.corpus import wordnet as wn
import numpy as np


class Buscador:
    def __init__(self, *args, **kwargs):
        self.g = Github(args[0], args[1])  # Fazendo login
        if "words" in kwargs:
            words = kwargs["words"]
        else:
            words = ("uml", "xmi", "diagram", "architecture", "design", "model")
        if "extensions" in kwargs:
            extensions = kwargs["extensions"]
        else:
            extensions = ("uml", "xmi", "xml", "bmp", "jpeg", "gif", "svg", "pdf", "png", "jpg")
        if "repos" in kwargs:
            repos = kwargs["repos"]
        else:
            repos = ("eclipse/emf.compare/", "Open-MBEE/Comodo/", "artist-project/ARTIST/", "GRA-UML/tool/",
                     "mdht/mdht-models/", "knot3/knot3-prototype/", "biddyweb/Andromedia/",
                     "edipofederle/architecture-representation/", "ClockWorkTeam/ClockWork/",
                     "eclipse/gmf-tooling.uml2tools/", "uidaho/cs383/", "google-code/alban/",
                     "andrewtron3000/embedded-ml/", "AlbanAndrieu/nabla-andromda/", "Marusyk/PatternsProjects/",
                     "juancadavid/papyrus/", "Gabitchov/org.eclipse.papyrus/", "unidal/garden/",
                     "Obeo/emf.compare.acceptance.tests/", "GufCab/Semester-Projekt---PC-Program/",
                     "mda-codegen/mda-codegen/", "artist-project/ARTIST-Orchestration_Tool/",
                     "Chinchilla-Software-Com/CQRS/", "GRA-UML/specification/", "eclipse/ocl/", "zactyy/cpts323/",
                     "zlphoenix/LearnCSharp/", "foutrelis/pacmarge-archive/", "MediaMotionCommunity/MediaMotion/",
                     "artist-project/ARTIST-MCF/", "muhamadnurawaludin/Crimezone/", "devinh/KinectRCCar/",
                     "luancarloswd/CoachingPlan/", "Marloncheg182/Examples/", "pabloariasmora/BattlePong/",
                     "artist-project/ARTIST-MDT/", "JohanBeekers/Jue_De_Barricade/", "ljvblfz/MicrosoftNLayerApp/",
                     "zlphoenix/MyDemos/", "RivaCode/BSpline3DGen/", "DevBoost/Reuseware/",
                     "tradi/TestAutomationFramework/", "crowdcode-de/KissMDA/", "dkaynarc/cygnus/",
                     "TesByRus/MusicXMLViewer/", "GiovaniGuizzo/OPLA-Patterns/", "knot3/CSharpUML/",
                     "adisandro/MMINT/", "info-sharing-environment/NIEM-Modeling-Tool/", "manisero/SemesterIX/",
                     "DistributedSystemGroup4/Distributed-Taxi-Central/", "maidaisy/GitHub/",
                     "ulanisMines/playmybatik/", "lofidewanto/dep-injection-examples/", "z3ss/SoftwareArchitecture/",
                     "jeevatkm/generic-repo/", "dresden-ocl/legacy/", "ivalop81/workspace/",
                     "ModelDriven/Alf-Reference-Implementation/", "artist-project/ARTIST-MUT/",
                     "jackjackrene/SAD_Pi/", "GerasimIG/SimpleSocialNetwork/",
                     "GufCab/Semester-Projekt---Pi-Program/", "TimHarrison1260/Dissertation/", "moliz/moliz.alf/",
                     "ampie/pavanecce/", "dmpyatin/SchedulerHost/", "visionnguyen/remote-desktop/",
                     "ATOffenburg/eCar/", "SINTEF-9012/bvr/", "swmuir/mdht/", "artist-project/ARTIST-TFT/",
                     "patrickneubauer/fuml-library-support/", "RLH78/CPTS_323_Project/", "Vycka/KTU.DataTransfer/",
                     "Jiwan/Civilisation/", "Omarmtz/Perceptual-Hash/", "Baspar/InteractiveBattleshipProject/",
                     "dinkelstefan/Project-Qars-Windesheim/", "GufCab/Semester-Projekt---Test-Kode/",
                     "pietermartin/umlg/", "a-jahanshahlo/Feedor/", "deib-polimi/Corretto/", "eye2web/StageManager/",
                     "mdht/mdht/", "MiaoF/MyVariabilityModelingFramework/", "N7Repository/ASML/", "dowe/Project2/",
                     "dresden-ocl/dresdenocl/", "IHTSDO/ISAAC/", "prilutskiy/black-jack/",
                     "RobotML/RobotML-SDK-Juno/", "sw505/code/", "TestingTechnologies/TTmodeler/",
                     "VA-CTT/ISAAC-fx-gui/", "WSU-323-Spring2015/Course/", "BlackhoefStudios/DropIt/",
                     "Kooboo/Ecommerce/", "kwf2030/go-package-diagram/")
        self.repos = set()  # Usado set por ser mais simples e compacto (habilidades de list irrelevantes para o caso)
        self.words = set()  # Importante uso de set para evitar duplicatas devido a consulta ao acervo WordNET
        self.extensions = set()
        for i in repos:
            self.repos.add(i)
        for i in words:
            for y in wn.synsets(i, wn.NOUN):  # Capturando sinônimos em wordnet
                w = str(y).split("'")[1].split(".")[0]  # Removendo metadados retornados pelo serviço
                self.words.add(w)
                self.words.add(w.capitalize())
                self.words.add(w.upper())
        for i in extensions:
            self.extensions.add(i)
            self.extensions.add(i.upper())

    def _hasWords(self, text):
        if any(y in text for y in self.words):
            return True
        return False

    def _hasExtensions(self, file_path):
        file_extension = file_path.rsplit(".", 1)[-1]
        if file_extension in self.extensions:
            return True
        return False

    def _buscaPullRequestsComments(self, pull_request):
        text = ""
        amount = 0
        for comment in pull_request.get_comments():  # percorrer comentarios do commit
            text += comment.id
            amount += 1
        return amount, text

    def _buscaPullRequests(self, repo):
        pull_requests_relevantes = []
        text = ""
        try:
            pull_requests = repo.get_pulls()
            for pull_request in pull_requests:  # percorrer commits do repositorio
                text += pull_request.text + pull_request.id
                n_comments, comment_text = self._buscaPullRequestsComments(pull_request)
                text += comment_text
                if self._hasWords(text):
                    pull_requests_relevantes.append(n_comments)
        except GithubException:  # Ignora exceções de repositórios vazios
            pass
        return pull_requests_relevantes

    def _buscaCommitsComments(self, commit):
        text = ""
        amount = 0
        for comment in commit.get_comments():  # percorrer comentarios do commit
            text += comment.id
            amount += 1
        return amount, text

    def _buscaCommits(self, repo):
        commits_relevantes = []
        text = ""
        try:
            commits = repo.get_commits_and_files()
            for commit in commits:  # percorrer commits do repositorio
                text += commit.commit.message
                n_comments, comment_text = self._buscaCommitsComments(commit)
                text += comment_text
                if self._hasWords(text):
                    commits_relevantes.append(n_comments)
        except GithubException:  # Ignora exceções de repositórios vazios
            pass
        return commits_relevantes

    def _buscaIssueComment(self, issue):
        text = ""  # evita que https://github.com/BlackhoefStudios/DropIt/issues/6 estrague o programa
        amount = 0
        for comment in issue.get_comments():  # percorrer comentarios do issue
            text += comment.id
            amount += 1
        return amount, text

    def _buscaIssue(self, repo):
        issues_relevantes = []
        text = ""
        try:
            for issue in repo.get_issues():  # percorrer issues do repositorio
                text += issue.id
                n_comments, comment_text = self._buscaIssueComment(issue)
                text += comment_text
                if self._hasWords(text):
                    issues_relevantes.append(n_comments)  # adiciona item a issues relevantes contendo n de comentarios
        except GithubException:  # Ignora exceções de inexistência de issues
            pass
        return issues_relevantes

    def _buscaArquivos(self, repo):
        arquivos_relevantes = 0
        try:
            files = repo.get_git_tree(repo.get_commits_and_files()[0].sha, recursive=True).tree
        except GithubException:
            pass  # ignorar exceções de pastas vazias
        else:
            for file in files:
                if self._hasWords(file.path) or self._hasExtensions(file.path):
                    arquivos_relevantes += 1
        return arquivos_relevantes

    def _correlacionaIssueCommit(self, issue):
        pass

    def _correlacionaIssuePRCommit(self, pull_request):
        pass

    def busca(self):
        total_pull_requests_relevantes = 0
        total_commits_relevantes = 0
        total_issues_relevantes = 0
        total_arquivos_relevantes = 0
        for repo in self.repos:  # percorrer todos respositorios da lista
            for repoSearchResult in self.g.search_repositories(repo):  # percorrer todos resultados da busca
                pull_requests_relevantes = self._buscaPullRequests(repoSearchResult)
                if len(pull_requests_relevantes) == 0:
                    comentarios_pull_requests_relevantes = 0
                else:
                    comentarios_pull_requests_relevantes = np.mean(pull_requests_relevantes)
                commits_relevantes = self._buscaCommits(repoSearchResult)
                if len(commits_relevantes) == 0:
                    comentarios_commits_relevantes = 0
                else:
                    comentarios_commits_relevantes = np.mean(commits_relevantes)
                issues_relevantes = self._buscaIssue(repoSearchResult)
                if len(issues_relevantes) == 0:
                    comentarios_issues_relevantes = 0
                else:
                    comentarios_issues_relevantes = np.mean(issues_relevantes)
                arquivos_relevantes = self._buscaArquivos(repoSearchResult)

                total_arquivos_relevantes += arquivos_relevantes
                total_commits_relevantes += len(commits_relevantes)
                total_issues_relevantes += len(issues_relevantes)
                total_pull_requests_relevantes += len(pull_requests_relevantes)
                print(
                    '''
                Repo: {}
                    commits relevantes: {}
                        média do número de comentários: {}
                    arquivos relevantes: {}
                    issues relevantes: {}
                        média do número de comentários: {}
                    pull requests relevantes: {}
                        média do número de comentários: {}
                    '''.format(repoSearchResult.name, len(commits_relevantes), comentarios_commits_relevantes,
                               arquivos_relevantes,
                               len(issues_relevantes), comentarios_issues_relevantes, len(pull_requests_relevantes),
                               comentarios_pull_requests_relevantes))
        print(
            '''
        Total:
            commits relevantes: {}
            arquivos relevantes: {}
            issues relevantes: {}
            pull requests relevantes: {}
            '''.format(total_commits_relevantes, total_arquivos_relevantes, total_issues_relevantes, total_pull_requests_relevantes))


if __name__ == "__main__":
    usuario = input("usuario")  # Preencher com usuario de sua conta GitHub
    senha = input("senha")  # Preencher com senha de sua conta GitHub
    buscador = Buscador(usuario, senha)
    buscador.busca()
